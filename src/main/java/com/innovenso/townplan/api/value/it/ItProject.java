package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.PassiveStructureElement;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.business.Enterprise;
import java.util.List;
import java.util.Set;

public interface ItProject extends PassiveStructureElement, HasImpact {
	Enterprise getEnterprise();

	List<ItProjectMilestone> getMilestones();

	@Override
	default String getType() {
		return "IT Project";
	}

	@Override
	default String getCategory() {
		return "IT Project";
	}

	Set<Relationship> getStakeholderRelationships();
}
