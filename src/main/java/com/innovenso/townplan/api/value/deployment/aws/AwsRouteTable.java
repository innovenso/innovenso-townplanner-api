package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsRouteTable extends AwsElement {
	AwsVpc getVpc();

	Set<AwsSubnet> getSubnets();
}
