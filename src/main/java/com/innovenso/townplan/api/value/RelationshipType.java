package com.innovenso.townplan.api.value;

import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.deployment.aws.AwsElement;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum RelationshipType {
	FLOW("flow", Set.of(ArchitectureBuildingBlock.class, ItSystem.class, ItContainer.class, BusinessActor.class), Set
			.of(ArchitectureBuildingBlock.class, ItSystem.class, ItContainer.class, BusinessActor.class)), REALIZATION(
					"realization",
					Set.of(BusinessCapability.class, ArchitectureBuildingBlock.class, ItSystem.class, ItPlatform.class),
					Set.of(BusinessCapability.class, ArchitectureBuildingBlock.class,
							ItSystemIntegration.class)), COMPOSITION(
									"composition", Set.of(Element.class),
									Set.of(Element.class)), STAKEHOLDER("stakeholder", Set.of(BusinessActor.class), Set
											.of(ItProject.class, ItProjectMilestone.class, ItSystem.class)), DELIVERY(
													"delivery", Set.of(BusinessActor.class),
													Set.of(ItSystem.class, ItContainer.class)), AWS_DEPENDENCY(
															"dependency", Set.of(AwsElement.class),
															Set.of(AwsElement.class)), IMPLEMENTATION("implements",
																	Set.of(AwsElement.class),
																	Set.of(ItContainer.class)), IMPACT_CREATE(
																			"impact - creates",
																			Set.of(ItProjectMilestone.class,
																					Decision.class,
																					DecisionOption.class),
																			Set.of(BusinessCapability.class,
																					ArchitectureBuildingBlock.class,
																					ItSystem.class, ItContainer.class,
																					DataEntity.class,
																					ItSystemIntegration.class)), IMPACT_REMAINS_UNCHANGED(
																							"impact - remains unchanged",
																							Set.of(ItProjectMilestone.class,
																									Decision.class,
																									DecisionOption.class),
																							Set.of(BusinessCapability.class,
																									ArchitectureBuildingBlock.class,
																									ItSystem.class,
																									ItContainer.class,
																									DataEntity.class,
																									ItSystemIntegration.class)), IMPACT_CHANGE(
																											"impact - changes",
																											Set.of(ItProjectMilestone.class,
																													Decision.class,
																													DecisionOption.class),
																											Set.of(BusinessCapability.class,
																													ArchitectureBuildingBlock.class,
																													ItSystem.class,
																													ItContainer.class,
																													DataEntity.class,
																													ItSystemIntegration.class)), IMPACT_REMOVE(
																															"impact - removes",
																															Set.of(ItProjectMilestone.class,
																																	Decision.class,
																																	DecisionOption.class),
																															Set.of(BusinessCapability.class,
																																	ArchitectureBuildingBlock.class,
																																	ItSystem.class,
																																	ItContainer.class,
																																	DataEntity.class,
																																	ItSystemIntegration.class)), RESPONSIBLE(
																																			"responsible",
																																			Set.of(BusinessActor.class),
																																			Set.of(ItProject.class,
																																					ItProjectMilestone.class,
																																					ItSystem.class,
																																					Principle.class,
																																					Decision.class)), ACCOUNTABLE(
																																							"accountable",
																																							Set.of(BusinessActor.class),
																																							Set.of(ItProject.class,
																																									ItProjectMilestone.class,
																																									Decision.class)), HAS_BEEN_CONSULTED(
																																											"consulted",
																																											Set.of(BusinessActor.class),
																																											Set.of(ItProject.class,
																																													ItProjectMilestone.class,
																																													Decision.class)), TO_BE_CONSULTED(
																																															"to be consulted",
																																															Set.of(BusinessActor.class),
																																															Set.of(ItProject.class,
																																																	ItProjectMilestone.class)), HAS_BEEN_INFORMED(
																																																			"informed",
																																																			Set.of(BusinessActor.class),
																																																			Set.of(ItProject.class,
																																																					ItProjectMilestone.class,
																																																					Decision.class)), TO_BE_INFORMED(
																																																							"to be informed",
																																																							Set.of(BusinessActor.class),
																																																							Set.of(ItProject.class,
																																																									ItProjectMilestone.class)), MEMBER(
																																																											"member",
																																																											Set.of(BusinessCapability.class,
																																																													ArchitectureBuildingBlock.class,
																																																													BusinessActor.class),
																																																											Set.of(Enterprise.class,
																																																													BusinessActor.class)), INFLUENCES(
																																																															"influences",
																																																															Set.of(Principle.class),
																																																															Set.of(Decision.class,
																																																																	DecisionOption.class)), HAS_ONE_OR_MORE(
																																																																			"has one or more",
																																																																			Set.of(DataEntity.class),
																																																																			Set.of(DataEntity.class)), HAS_ZERO_OR_MORE(
																																																																					"has zero or more",
																																																																					Set.of(DataEntity.class),
																																																																					Set.of(DataEntity.class)), HAS_ZERO_OR_ONE(
																																																																							"has zero or one",
																																																																							Set.of(DataEntity.class),
																																																																							Set.of(DataEntity.class)), HAS_ONE(
																																																																									"has one",
																																																																									Set.of(DataEntity.class),
																																																																									Set.of(DataEntity.class));

	public final String label;
	public final Set<Class<? extends Concept>> allowedSourceConceptTypes;
	public final Set<Class<? extends Concept>> allowedTargetConceptTypes;

	public String getLabel() {
		return label;
	}

	RelationshipType(String label, Set<Class<? extends Concept>> allowedSourceConceptTypes,
			Set<Class<? extends Concept>> allowedTargetConceptTypes) {
		this.label = label;
		this.allowedSourceConceptTypes = allowedSourceConceptTypes;
		this.allowedTargetConceptTypes = allowedTargetConceptTypes;
	}

	public static Optional<RelationshipType> getRelationshipType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(RelationshipType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<RelationshipType> getImpactTypes() {
		return Set.of(IMPACT_CHANGE, IMPACT_CREATE, IMPACT_REMOVE);
	}

	public static boolean isImpact(RelationshipType relationshipType) {
		return getImpactTypes().contains(relationshipType);
	}

	public boolean isAllowedSource(Concept concept) {
		return allowedSourceConceptTypes.stream().anyMatch(allowedConcept -> allowedConcept.isInstance(concept));
	}

	public boolean isAllowedTarget(Concept concept) {
		return allowedTargetConceptTypes.stream().anyMatch(allowedConcept -> allowedConcept.isInstance(concept));
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
