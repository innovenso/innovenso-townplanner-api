package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsAutoScalingGroup extends AwsElement {
	AwsVpc getVpc();

	Set<AwsAvailabilityZone> getAvailabilityZones();

	Set<AwsSubnet> getSubnets();

	Set<AwsInstance> getInstances();
}
