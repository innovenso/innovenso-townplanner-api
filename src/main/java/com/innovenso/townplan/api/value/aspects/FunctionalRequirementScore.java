package com.innovenso.townplan.api.value.aspects;

import java.util.Optional;

public interface FunctionalRequirementScore extends ModelAspect {
	String getDescription();

	FunctionalRequirement getFunctionalRequirement();

	ScoreWeight getWeight();

	@Override
	default Optional<ModelAspect> getParent() {
		return Optional.of(getFunctionalRequirement());
	}
}
