package com.innovenso.townplan.api.value.view;

import com.innovenso.townplan.api.value.strategy.BusinessCapability;

public interface ImpactBusinessCapability {
	BusinessCapability getCapability();

	boolean isImpacted();
}
