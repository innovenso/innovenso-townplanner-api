package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import java.util.Set;

public interface ItPlatform extends ActiveStructureElement, HasTechnologies {
	@Override
	default String getType() {
		return "Platform";
	}

	Set<ItSystem> getSystems();

	Set<ItContainer> getContainers();

	Set<ArchitectureBuildingBlock> getBuildingBlocks();

	default boolean hasSystems() {
		return !getSystems().isEmpty();
	}

	default boolean hasContainers() {
		return !getContainers().isEmpty();
	}
}
