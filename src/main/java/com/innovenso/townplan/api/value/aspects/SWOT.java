package com.innovenso.townplan.api.value.aspects;

public interface SWOT extends ModelAspect {
	SWOTType getType();

	String getDescription();
}
