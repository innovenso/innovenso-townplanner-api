package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import java.util.Set;
import java.util.stream.Collectors;

public interface ArchitectureBuildingBlock extends ActiveStructureElement {
	Enterprise getEnterprise();

	@Override
	default String getType() {
		return "Architecture Building Block";
	}

	@Override
	default String getCategory() {
		return "IT Architecture";
	}

	default Set<BusinessCapability> getRealizedCapabilities() {
		return getDirectDependencies(BusinessCapability.class).stream().filter(BusinessCapability.class::isInstance)
				.map(BusinessCapability.class::cast).collect(Collectors.toSet());
	}

	default Set<ItSystem> getRealizingSystems() {
		return getDirectDependencies(ItSystem.class).stream().filter(ItSystem.class::isInstance)
				.map(ItSystem.class::cast).collect(Collectors.toSet());
	}
}
