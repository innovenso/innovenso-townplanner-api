package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum TechnologyRecommendation {
	ADOPT("Adopt", 0), TRIAL("Trial", 1), ASSESS("Assess", 2), HOLD("Hold", 3), UNDEFINED("Undefined", 4);

	public final String label;
	public final int ring;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	TechnologyRecommendation(String label, int ring) {
		this.label = label;
		this.ring = ring;
	}

	public static Optional<TechnologyRecommendation> getTechnologyRecommendation(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(TechnologyRecommendation.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public int getRadarRing() {
		return ring;
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}

	public static List<TechnologyRecommendation> getRadarSections() {
		return List.of(ADOPT, TRIAL, ASSESS, HOLD);
	}
}
