package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum SecurityImpactLevel {
	LOW("Low"), MEDIUM("Medium"), HIGH("High"), EXTREME("Extremely high");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	SecurityImpactLevel(String label) {
		this.label = label;
	}

	public static Optional<SecurityImpactLevel> getSecurityImpactLevel(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(SecurityImpactLevel.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
