package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Optional;

public interface AwsElasticIpAddress extends AwsElement {
	String getAllocationId();

	String getAssociationId();

	String getCarrierIp();

	String getCustomerOwnedIp();

	String getCustomerOwnedIpv4Pool();

	String getDomain();

	String getInstanceId();

	String getNetworkBorderGroup();

	String getNetworkInterfaceId();

	String getNetworkInterfaceOwnerId();

	String getPrivateIpAddress();

	String getPublicIpAddress();

	String getPublicIpv4Pool();

	String getRegion();

	AwsAccount getAccount();

	Optional<AwsInstance> getInstance();
}
