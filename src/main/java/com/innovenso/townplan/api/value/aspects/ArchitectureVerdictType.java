package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ArchitectureVerdictType {
	TOLERATE("Tolerate"), INVEST("Invest"), MIGRATE("Migrate"), ELIMINATE("Eliminate"), UNKNOWN("Unknown");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	ArchitectureVerdictType(String label) {
		this.label = label;
	}

	public static Optional<ArchitectureVerdictType> getArchitectureVerdictType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ArchitectureVerdictType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
