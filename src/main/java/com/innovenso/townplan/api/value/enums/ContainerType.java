package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ContainerType {
	MICROSERVICE("microservice"), SERVICE("service"), FUNCTION("function"), DATABASE("database"), FILESYSTEM(
			"filesystem"), QUEUE("queue"), TOPIC("topic"), STREAM("stream"), GATEWAY("gateway"), PROXY(
					"proxy"), FIREWALL("firewall"), WEB_UI("web-ui"), MOBILE_UI("mobile-ui"), TV_UI(
							"tv-ui"), WATCH_UI("watch-ui"), TERMINAL_UI(
									"terminal-ui"), DESKTOP_UI("desktop-ui"), BATCH("batch"), OTHER("container");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	ContainerType(String label) {
		this.label = label;
	}

	public static Optional<ContainerType> getContainerType(final String value) {
		if (value == null)
			return Optional.of(OTHER);
		try {
			return Optional.of(ContainerType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.of(OTHER);
		}
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
