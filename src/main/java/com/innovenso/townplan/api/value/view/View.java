package com.innovenso.townplan.api.value.view;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.ModelComponent;
import com.innovenso.townplan.api.value.Relationship;
import java.util.Set;

public interface View extends ModelComponent {
	String getTitle();

	String getDescription();

	String getCategory();

	Set<Element> getElements();

	<T extends Element> Set<T> getElements(Class<T> elementClass);

	Set<Relationship> getRelationships();
}
