package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsSubnet extends AwsElement {
	AwsVpc getVpc();

	AwsAvailabilityZone getAvailabilityZone();

	String getSubnetMask();

	Set<AwsInstance> getInstances();

	boolean isPublicSubnet();
}
