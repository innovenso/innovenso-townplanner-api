package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum SecurityImpactType {
	CONFIDENTIALITY("Confidentiality", "A"), INTEGRITY("Integrity", "B"), AVAILABILITY("Availability",
			"C"), DATA_LOSS("Data Loss", "D");

	public final String label;
	public final String sortKey;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	SecurityImpactType(String label, String sortKey) {
		this.label = label;
		this.sortKey = sortKey;
	}

	public static Optional<SecurityImpactType> getSecurityImpactType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(SecurityImpactType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
