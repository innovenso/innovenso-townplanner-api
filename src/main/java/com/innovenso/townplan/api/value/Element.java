package com.innovenso.townplan.api.value;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface Element extends Concept {
	Set<Element> getDirectDependencies();

	Set<Element> getDirectDependencies(Class<? extends Element> elementClass);

	Set<Element> getDirectIncomingDependencies();

	Set<Element> getDirectOutgoingDependencies();

	Set<Element> getChildDependencies();

	Set<Element> getChildDependencies(Class<? extends Element> elementClass);

	Set<Relationship> getIncomingRelationships();

	Set<Relationship> getOutgoingRelationships();

	Set<Relationship> getIncomingRelationships(Class<? extends Element> sourceClass);

	Set<Relationship> getOutgoingRelationships(Class<? extends Element> targetClass);

	Set<Relationship> getRelationships(Class<? extends Element> otherClass);

	Set<Relationship> getRelationships();

	boolean acceptsIncomingDependency(Class<? extends Element> elementClass);

	boolean acceptsOutgoingDependency(Class<? extends Element> elementClass);

	Set<Class<? extends Element>> getAcceptedIncomingDependencies();

	Set<Class<? extends Element>> getAcceptedOutgoingDependencies();

	boolean acceptsChild(Class<? extends Element> childClass);

	Set<Class<? extends Element>> getAcceptedChildren();

	Set<Element> getChildren();

	Optional<Element> getParent();

	boolean hasChildren();

	default Set<RelationshipType> getAcceptedIncomingRelationshipTypes() {
		return Arrays.stream(RelationshipType.values())
				.filter(relationshipType -> relationshipType.isAllowedTarget(this)).collect(Collectors.toSet());
	}

	default Set<RelationshipType> getAcceptedOutgoingRelationshipTypes() {
		return Arrays.stream(RelationshipType.values())
				.filter(relationshipType -> relationshipType.isAllowedSource(this)).collect(Collectors.toSet());
	}
}
