package com.innovenso.townplan.api.value.it.principle;

import com.innovenso.townplan.api.value.PassiveStructureElement;
import com.innovenso.townplan.api.value.business.Enterprise;

public interface Principle extends PassiveStructureElement {
	Enterprise getEnterprise();

	@Override
	default String getCategory() {
		return "Principle";
	}
}
