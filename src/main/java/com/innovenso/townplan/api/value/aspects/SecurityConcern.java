package com.innovenso.townplan.api.value.aspects;

public interface SecurityConcern extends ModelAspect {
	String getTitle();

	String getDescription();

	SecurityConcernType getConcernType();

	String getSortKey();
}
