package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum AwsInstanceType {
	EC2("EC2 instance"), LAMBDA("Lambda function"), DYNAMODB("DynamoDB Table"), DYNAMO_STREAM(
			"DynamoDB Stream"), API_GATEWAY("API Gateway"), S3("S3 Bucket"), SQS("SQS Queue"), SNS("SNS Topic"), RDS(
					"RDS Instance"), EVENT_BRIDGE("Event Bridge"), APP_SYNC("AppSync"), ECS_TASK(
							"ECS Task Definition"), ECS_SERVICE("ECS Service"), ECS_CLUSTER("ECS Cluster"), KINESIS(
									"Kinesis Stream"), ELASTIC_SEARCH("AWS ElasticSearch"), CLOUDWATCH(
											"AWS CloudWatch"), ELB("Elastic Load Balancer"), ELB_LISTENER(
													"ELB Listener"), ELB_RULE("ELB Rule"), TARGET_GROUP(
															"Target Group"), EBS("Elastic Block Store volume"), IGW(
																	"Internet Gateway"), TGW("Transit Gateway"), VPGW(
																			"Virtual Private Gateway"), NAT(
																					"NAT Gateway"), ROUTER(
																							"VPC Router"), BEANSTALK(
																									"Elastic Beanstalk Environment"), ELASTICACHE_REPLICATION(
																											"Elasticache Replication Group"), ELASTICACHE_CLUSTER(
																													"Elasticache Redis Cluster"), VPCE(
																															"VPC Endpoint"), UNKNOWN(
																																	"Unknown");

	public final String label;

	public String getLabel() {
		return label;
	}

	AwsInstanceType(String label) {
		this.label = label;
	}

	public static AwsInstanceType getAwsInstanceType(final String value) {
		if (value == null)
			return AwsInstanceType.UNKNOWN;
		try {
			return AwsInstanceType.valueOf(value.toUpperCase());
		} catch (IllegalArgumentException e) {
			return AwsInstanceType.UNKNOWN;
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
