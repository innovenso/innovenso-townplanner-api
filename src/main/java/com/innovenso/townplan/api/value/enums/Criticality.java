package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum Criticality {
	UNKNOWN("Unknown"), VERY_LOW("Very Low"), LOW("Low"), MEDIUM("Medium"), HIGH("High"), VERY_HIGH("Very High");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	Criticality(String label) {
		this.label = label;
	}

	public static Optional<Criticality> getCriticality(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(Criticality.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
