package com.innovenso.townplan.api.value.aspects;

public interface QualityAttributeRequirement extends ModelAspect {
	String getTitle();

	String getSourceOfStimulus();

	String getStimulus();

	String getEnvironment();

	String getResponse();

	String getResponseMeasure();

	String getSortKey();

	RequirementWeight getWeight();
}
