package com.innovenso.townplan.api.value;

import com.innovenso.townplan.api.value.aspects.ContentDistribution;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import java.io.File;
import java.util.Set;
import org.apache.commons.lang3.text.WordUtils;

public interface ModelComponent {
	String getKey();

	default String getSortKey() {
		return getKey();
	}

	File getOutputFile(String provider, String format, String view);

	void setOutputFile(String provider, String format, String view, File outputFile);

	Set<File> getOutputFiles();

	Set<ContentDistribution> getContentDistributions();

	Set<ContentDistribution> getContentDistributions(ContentOutputType contentOutputType);

	default String getCamelCasedKey() {
		return WordUtils.capitalizeFully(getKey(), '-', '_').replaceAll("-", "").replaceAll("_", "")
				.replaceAll("0", "Zero").replaceAll("1", "One").replaceAll("2", "Two").replaceAll("3", "Three")
				.replaceAll("4", "Four").replaceAll("5", "Five").replaceAll("6", "Six").replaceAll("7", "Seven")
				.replaceAll("8", "Eight").replaceAll("9", "Nine");
	}
}
