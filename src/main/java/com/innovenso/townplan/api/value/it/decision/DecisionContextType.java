package com.innovenso.townplan.api.value.it.decision;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum DecisionContextType {
	CURRENT_STATE("Current State"), ASSUMPTION("Assumption"), GOAL("Goal"), CONSEQUENCE("Consequence");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	DecisionContextType(String label) {
		this.label = label;
	}

	public static Optional<DecisionContextType> getDecisionContextType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(DecisionContextType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}

	public static List<DecisionContextType> getDecisionContextTypes() {
		return List.of(values());
	}
}
