package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.PassiveStructureElement;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.enums.EntityType;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import java.util.List;

public interface DataEntity extends PassiveStructureElement {
	ItSystem getMasterSystem();

	String getSensitivity();

	String getCommercialValue();

	boolean isConsentNeeded();

	Enterprise getEnterprise();

	BusinessCapability getBusinessCapability();

	List<DataEntityField> getFields();

	EntityType getEntityType();

	@Override
	default String getCategory() {
		return "DataEntity";
	}
}
