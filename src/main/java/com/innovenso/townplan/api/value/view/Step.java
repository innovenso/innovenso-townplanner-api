package com.innovenso.townplan.api.value.view;

import com.innovenso.townplan.api.value.Relationship;
import java.util.Optional;

public interface Step {
	Relationship getRelationship();

	Optional<String> getTitle();

	String getActualTitle();

	boolean isResponse();

	String getLabel();
}
