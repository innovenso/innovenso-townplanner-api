package com.innovenso.townplan.api.value.aspects;

import java.util.Optional;

public interface QualityAttributeRequirementScore extends ModelAspect {
	String getDescription();

	QualityAttributeRequirement getQualityAttributeRequirement();

	ScoreWeight getWeight();

	@Override
	default Optional<ModelAspect> getParent() {
		return Optional.of(getQualityAttributeRequirement());
	}
}
