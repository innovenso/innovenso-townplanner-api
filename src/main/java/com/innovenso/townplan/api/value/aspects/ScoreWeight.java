package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ScoreWeight {
	EXCEEDS_EXPECTATIONS("Exceeds expectations", 3), MEETS_EXPECTATIONS("Meets expectations",
			2), ALMOST_MEETS_EXPECTATIONS("Almost meets expectations",
					1), DOES_NOT_MEET_EXPECTATIONS("Does not meet expectations at all", 0), UNKNOWN("Unknown", 1);

	public final String label;
	public final int weight;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	public int getWeight() {
		return weight;
	}

	ScoreWeight(String label, int weight) {
		this.label = label;
		this.weight = weight;
	}

	public static Optional<ScoreWeight> getScoreWeight(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ScoreWeight.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
