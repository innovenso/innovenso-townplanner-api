package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum LifecyleType {
	PLANNED("planned"), ACTIVE("active"), PHASEOUT("phaseout"), DECOMMISSIONED("decommissioned"), UNKNOWN("unknown");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	LifecyleType(String label) {
		this.label = label;
	}

	public static Optional<LifecyleType> getLifecycleType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(LifecyleType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
