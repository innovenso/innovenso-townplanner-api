package com.innovenso.townplan.api.value.aspects;

public interface SecurityImpact extends ModelAspect {
	SecurityImpactType getImpactType();

	String getDescription();

	SecurityImpactLevel getImpactLevel();

	default String getSortKey() {
		return getImpactType().sortKey;
	}
}
