package com.innovenso.townplan.api.value.aspects;

public interface Constraint extends ModelAspect {
	String getTitle();

	String getDescription();

	String getSortKey();

	RequirementWeight getWeight();
}
