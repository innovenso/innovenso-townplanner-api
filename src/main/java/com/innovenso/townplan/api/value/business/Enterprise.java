package com.innovenso.townplan.api.value.business;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public interface Enterprise extends ActiveStructureElement {
	@Override
	default String getType() {
		return "Enterprise";
	}

	@Override
	default String getCategory() {
		return "Enterprise";
	}

	default List<BusinessCapability> getBusinessCapabilities() {
		return getChildren().stream().filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
				.filter(cap -> cap.getLevel() == 0).sorted(Comparator.comparing(BusinessCapability::getSortKey))
				.collect(Collectors.toList());
	}
}
