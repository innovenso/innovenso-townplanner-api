package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.PassiveStructureElement;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.business.BusinessActor;
import java.util.Set;

public interface ItProjectMilestone extends PassiveStructureElement, HasImpact {
	ItProject getProject();

	@Override
	default String getType() {
		return "IT Project Milestone";
	}

	@Override
	default String getCategory() {
		return "IT Project";
	}

	Set<Relationship> getStakeholderRelationships();

	Set<BusinessActor> getResponsibles();

	Set<BusinessActor> getAccountables();

	Set<BusinessActor> getConsulted();

	Set<BusinessActor> getInformed();

	Set<BusinessActor> getToBeConsulted();

	Set<BusinessActor> getToBeInformed();

	Set<BusinessActor> getChickens();

	Set<BusinessActor> getPigs();

	Set<BusinessActor> getRaci();
}
