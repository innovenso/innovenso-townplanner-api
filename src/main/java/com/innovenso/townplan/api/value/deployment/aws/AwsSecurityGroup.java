package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;

public interface AwsSecurityGroup extends AwsElement {
	Set<AwsInstance> getInstances();

	default boolean hasInstances(final @NonNull String subnetKey) {
		return getInstances().stream().flatMap(awsInstance -> awsInstance.getSubnets().stream())
				.anyMatch(subnet -> subnet.getKey().equals(subnetKey));
	}

	default Set<AwsInstance> getInstances(final @NonNull String subnetKey) {
		return getInstances().stream().filter(awsInstance -> !awsInstance.getSubnets().isEmpty()).filter(
				instance -> instance.getSubnets().stream().anyMatch(subnet -> subnet.getKey().equals(subnetKey)))
				.collect(Collectors.toSet());
	}

	AwsVpc getVpc();
}
