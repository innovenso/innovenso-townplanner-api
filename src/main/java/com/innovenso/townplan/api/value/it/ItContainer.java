package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import java.util.Set;
import java.util.stream.Collectors;

import com.innovenso.townplan.api.value.enums.ContainerType;
import lombok.NonNull;

public interface ItContainer extends ActiveStructureElement, HasTechnologies {
	ItSystem getSystem();

	Set<Technology> getTechnologies();

	@Override
	default String getType() {
		return getContainerType().getLabel();
	}

	ContainerType getContainerType();

	default String getTechnologyDescription() {
		if (getTechnologies() == null || getTechnologies().isEmpty())
			return "";
		else
			return getTechnologies().stream().map(Technology::getTitle).collect(Collectors.joining(", "));
	}

	boolean hasTechnology(@NonNull final String key);
}
