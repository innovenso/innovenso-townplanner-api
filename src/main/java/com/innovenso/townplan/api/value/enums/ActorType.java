package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ActorType {
	NOUN("noun"), INDIVIDUAL("individual"), TEAM("team"), ORGANISATION("organisation"), OTHER("actor");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	ActorType(String label) {
		this.label = label;
	}

	public static Optional<ActorType> getActorType(final String value) {
		if (value == null)
			return Optional.of(OTHER);
		try {
			return Optional.of(ActorType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.of(OTHER);
		}
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
