package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum SWOTType {
	STRENGTH("strength"), WEAKNESS("weakness"), OPPORTUNITY("opportunity"), THREAT("threat");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return this.name();
	}

	SWOTType(String label) {
		this.label = label;
	}

	public static Optional<SWOTType> getSwotType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(SWOTType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static List<SWOTType> getSwotTypes() {
		return Arrays.stream(values()).collect(Collectors.toList());
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
