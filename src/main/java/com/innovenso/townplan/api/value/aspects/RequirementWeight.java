package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum RequirementWeight {
	MUST_HAVE("Must have - non-negotiable", 50), SHOULD_HAVE("Should have", 13), COULD_HAVE("Could have",
			8), WOULD_HAVE("Would have", 3), UNKNOWN("Unknown", 1);

	public final String label;
	public final int weight;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	public int getWeight() {
		return weight;
	}

	RequirementWeight(String label, int weight) {
		this.label = label;
		this.weight = weight;
	}

	public static Optional<RequirementWeight> getRequirementWeight(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(RequirementWeight.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
