package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ContentOutputType {
	SVG("SVG"), LATEX("LaTeX"), BLENDER("Blender"), ARCHIMATE("Archimate"), EXCEL("Excel"), PNG("PNG"), VISIO(
			"Visio"), ZIP("Zip"), JSON("JSON"), DOT("Graphviz"), VISJS(
					"VisJS"), CONFLUENCE_MARKUP("Confluence Markup"), CONFLUENCE_URL(
							"Confluence URL"), MIRO_URL("Miro URL"), ATTACHMENT("Attachment");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	ContentOutputType(String label) {
		this.label = label;
	}

	public static Optional<ContentOutputType> getContentOutputType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ContentOutputType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
