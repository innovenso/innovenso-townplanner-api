package com.innovenso.townplan.api.value.deployment.aws;

import com.innovenso.townplan.api.value.enums.AwsInstanceType;
import java.util.Optional;
import java.util.Set;

public interface AwsInstance extends AwsElement {
	AwsInstanceType getInstanceType();

	Optional<AwsVpc> getVpc();

	AwsAccount getAccount();

	Set<AwsSecurityGroup> getSecurityGroups();

	Optional<AwsAutoScalingGroup> getAutoScalingGroup();

	Set<AwsSubnet> getSubnets();

	Optional<AwsRegion> getRegion();
}
