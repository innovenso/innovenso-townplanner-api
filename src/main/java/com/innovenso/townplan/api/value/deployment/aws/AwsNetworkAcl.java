package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsNetworkAcl extends AwsElement {
	AwsVpc getVpc();

	Set<AwsSubnet> getSubnets();
}
