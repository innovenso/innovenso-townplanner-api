package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsVpc extends AwsElement {
	AwsRegion getRegion();

	AwsAccount getAccount();

	Set<AwsAvailabilityZone> getAvailabilityZones();

	Set<AwsSubnet> getSubnets(AwsAvailabilityZone availabilityZone);

	Set<AwsNetworkAcl> getNetworkAcls();

	String getNetworkMask();

	Set<AwsRouteTable> getRouteTables();

	Set<AwsSecurityGroup> getSecurityGroups();

	Set<AwsInstance> getInstances();

	Set<AwsAutoScalingGroup> getAutoScalingGroups();
}
