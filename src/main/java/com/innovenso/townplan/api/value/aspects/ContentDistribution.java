package com.innovenso.townplan.api.value.aspects;

import java.util.Optional;

public interface ContentDistribution extends ModelAspect {
	String getUrl();

	ContentOutputType getOutputType();

	Optional<String> getTitle();

	default String getSortKey() {
		return getTitle().orElse("");
	}
}
