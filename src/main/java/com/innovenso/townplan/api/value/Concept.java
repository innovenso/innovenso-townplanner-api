package com.innovenso.townplan.api.value;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.value.aspects.*;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.lang3.text.WordUtils;

public interface Concept extends ModelComponent {
	String getType();

	String getTitle();

	String getDescription();

	default String getDescriptionWrapped(int length) {
		return WordUtils.wrap(getDescription(), length);
	}

	String getCategory();

	Set<SWOT> getSwots();

	Set<SWOT> getSwots(Optional<SWOTType> swotType);

	Set<SWOT> getSwots(SWOTType swotType);

	List<SWOTType> getSwotTypes();

	default String getSortKeyByType() {
		return getCategory() + "_" + getKey();
	}

	Lifecycle getLifecycle();

	Lifecycle getLifecycle(KeyPointInTime keyPointInTime);

	boolean isActive(KeyPointInTime keyPointInTime);

	boolean isPlanned(KeyPointInTime keyPointInTime);

	boolean isPhasingOut(KeyPointInTime keyPointInTime);

	boolean isDecommissioned(KeyPointInTime keyPointInTime);

	boolean isUnknown(KeyPointInTime keyPointInTime);

	ArchitectureVerdict getArchitectureVerdict();

	default String getTextRepresentation() {
		return getTitle() + " (" + getType() + ")";
	}

	Set<Documentation> getDocumentations();

	Set<Cost> getCosts();

	Set<FatherTime> getFatherTimes();

	Set<Documentation> getDocumentations(Optional<DocumentationType> documentationType);

	Set<SecurityConcern> getSecurityConcerns();

	Set<SecurityImpact> getSecurityImpacts();

	Set<FunctionalRequirement> getFunctionalRequirements();

	Set<QualityAttributeRequirement> getQualityAttributes();

	Set<FunctionalRequirementScore> getFunctionalRequirementScores();

	Set<QualityAttributeRequirementScore> getQualityAttributeRequirementScores();

	Set<ConstraintScore> getConstraintScores();

	Set<Attachment> getAttachments();

	Set<Constraint> getConstraints();

	Set<ExternalId> getExternalIds();

	Optional<ModelAspect> getAspect(String aspectKey);

	boolean existsAt(KeyPointInTime keyPointInTime);

	int getMaximumTotalScore();

	int getMaximumTotalFunctionalScore();

	int getMaximumTotalQualityAttributeScore();

	int getMaximumTotalConstraintScore();

	int getTotalScore();

	int getTotalFunctionalScore();

	int getTotalQualityAttributeScore();

	int getTotalConstraintScore();

	int getTotalScoreAsPercentage();

	ScoreWeight getFunctionalRequirementScore(String requirementKey);

	ScoreWeight getQualityAttributeScore(String requirementKey);

	ScoreWeight getConstraintScore(String requirementKey);
}
