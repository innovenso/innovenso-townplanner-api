package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.enums.TechnologyType;
import java.util.Set;

public interface HasTechnologies {
	Set<Technology> getTechnologies(TechnologyType technologyType);

	Set<Technology> getTechnologies();
}
