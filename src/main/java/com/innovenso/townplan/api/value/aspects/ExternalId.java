package com.innovenso.townplan.api.value.aspects;

public interface ExternalId extends ModelAspect {
	ExternalIdType getExternalIdType();

	String getValue();
}
