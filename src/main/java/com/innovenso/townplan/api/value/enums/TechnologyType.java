package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum TechnologyType {
	LANGUAGE_FRAMEWORK("Languages & Frameworks", 0), PLATFORM("Platforms", 1), TECHNIQUE("Techniques", 2), TOOL("Tools",
			3), UNDEFINED("Undefined", 4);

	public final String label;
	public final int quadrant;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	TechnologyType(String label, int quadrant) {
		this.label = label;
		this.quadrant = quadrant;
	}

	public static Optional<TechnologyType> getTechnologyType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(TechnologyType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public int getRadarQuadrant() {
		return quadrant;
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}

	public static List<TechnologyType> getQuadrants() {
		return List.of(TECHNIQUE, TOOL, PLATFORM, LANGUAGE_FRAMEWORK);
	}
}
