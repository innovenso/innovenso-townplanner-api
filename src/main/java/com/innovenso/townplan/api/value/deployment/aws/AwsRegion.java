package com.innovenso.townplan.api.value.deployment.aws;

import java.util.Set;

public interface AwsRegion extends AwsElement {
	Set<AwsAvailabilityZone> getAvailabilityZones();

	Set<AwsVpc> getVpcs();

	Set<AwsInstance> getInstances();

	default boolean hasVpcs() {
		return getVpcs() != null && !getVpcs().isEmpty();
	}

	default boolean hasAvailabilityZones() {
		return getAvailabilityZones() != null && !getAvailabilityZones().isEmpty();
	}
}
