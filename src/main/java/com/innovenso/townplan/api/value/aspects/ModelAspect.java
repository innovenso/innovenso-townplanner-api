package com.innovenso.townplan.api.value.aspects;

import java.util.Optional;

public interface ModelAspect {
	String getKey();

	default String getSortKey() {
		return getKey();
	}

	default Optional<ModelAspect> getParent() {
		return Optional.empty();
	}
}
