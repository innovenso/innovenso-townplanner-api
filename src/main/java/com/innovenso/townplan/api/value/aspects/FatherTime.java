package com.innovenso.townplan.api.value.aspects;

import com.innovenso.townplan.api.KeyPointInTime;
import java.time.LocalDate;
import lombok.NonNull;

public interface FatherTime extends ModelAspect {
	FatherTimeType getFatherTimeType();

	LocalDate getPointInTime();

	String getTitle();

	String getDescription();

	default boolean isAfterOrEqual(@NonNull final LocalDate date) {
		return getPointInTime().isAfter(date) || getPointInTime().isEqual(date);
	}

	default boolean isBeforeOrEqual(@NonNull final LocalDate date) {
		return getPointInTime().isBefore(date) || getPointInTime().isEqual(date);
	}

	default boolean isAfterOrEqual(@NonNull final KeyPointInTime keyPointInTime) {
		return isAfterOrEqual(keyPointInTime.getDate());
	}

	default boolean isBeforeOrEqual(@NonNull final KeyPointInTime keyPointInTime) {
		return isBeforeOrEqual(keyPointInTime.getDate());
	}

	default boolean isBetween(@NonNull final KeyPointInTime first, @NonNull final KeyPointInTime last) {
		return isAfterOrEqual(first) && isBeforeOrEqual(last);
	}
}
