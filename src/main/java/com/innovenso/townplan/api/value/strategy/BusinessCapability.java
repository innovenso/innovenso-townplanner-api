package com.innovenso.townplan.api.value.strategy;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface BusinessCapability extends ActiveStructureElement {
	Enterprise getEnterprise();

	default Optional<BusinessCapability> getParentCapability() {
		return getParent().filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast);
	}

	default List<BusinessCapability> getChildCapabilities() {
		return getChildren().stream().filter(BusinessCapability.class::isInstance).map(BusinessCapability.class::cast)
				.sorted(Comparator.comparing(BusinessCapability::getSortKey)).collect(Collectors.toList());
	}

	default Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return getDirectDependencies(ArchitectureBuildingBlock.class).stream()
				.filter(ArchitectureBuildingBlock.class::isInstance).map(ArchitectureBuildingBlock.class::cast)
				.collect(Collectors.toSet());
	}

	default Set<ArchitectureBuildingBlock> getDescendantBuildingBlocks() {
		return getDescendantStream().flatMap(cap -> cap.getBuildingBlocks().stream()).collect(Collectors.toSet());
	}

	default Set<ArchitectureBuildingBlock> getBuildingBlocksIncludingDescendants() {
		return Sets.union(getBuildingBlocks(), getDescendantBuildingBlocks());
	}

	Set<BusinessCapability> getDescendantCapabilities();

	Stream<BusinessCapability> getDescendantStream();

	int getLevel();
}
