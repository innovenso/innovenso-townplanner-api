package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface ItSystem extends ActiveStructureElement, HasTechnologies {
	@Override
	default String getType() {
		return "System";
	}

	Optional<ItPlatform> getPlatform();

	Set<ItContainer> getContainers();

	Set<ItSystemIntegration> getIntegrations();

	default boolean hasIntegrations() {
		return getIntegrations() != null && getIntegrations().size() > 0;
	}

	default boolean hasContainers() {
		return getContainers() != null && getContainers().size() > 0;
	}

	default Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return getDirectDependencies(ArchitectureBuildingBlock.class).stream()
				.filter(ArchitectureBuildingBlock.class::isInstance).map(ArchitectureBuildingBlock.class::cast)
				.collect(Collectors.toSet());
	}
}
