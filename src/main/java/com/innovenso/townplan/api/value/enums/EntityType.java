package com.innovenso.townplan.api.value.enums;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum EntityType {
	ENTITY("Entity", "Logical information model"), VALUE_OBJECT("Value Object", "Logical information model"), COMMAND(
			"Command", "Logical information model"), EVENT("Event", "Logical information model"), QUERY("Query",
					"Logical information model"), READ_MODEL("Read Model (a.k.a. Projection)",
							"Logical information model"), CONCEPT("Concept",
									"Conceptual information model"), UNDEFINED("Undefined", "Undefined");

	public final String label;
	public final String category;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	public String getCategory() {
		return category;
	}

	public String getDescription() {
		return label + " (" + category + ")";
	}

	EntityType(String label, String category) {
		this.label = label;
		this.category = category;
	}

	public static Optional<EntityType> getEntityType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(EntityType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
