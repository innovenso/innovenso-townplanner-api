package com.innovenso.townplan.api.value;

import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.it.Technology;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface Relationship extends Concept {

	Element getSource();

	Element getTarget();

	Set<Technology> getTechnologies();

	boolean hasTechnology(String technologyKey);

	@Deprecated
	Optional<ItSystemIntegration> getSystemIntegration();

	default Integer getInfluence() {
		return 100;
	}

	default Integer getInterest() {
		return 100;
	}

	boolean isBidirectional();

	RelationshipType getRelationshipType();

	default boolean isCircular() {
		return Objects.equals(getSource(), getTarget());
	}

	default boolean has(Element element) {
		return Objects.equals(element, getSource()) || Objects.equals(element, getTarget());
	}

	default Element getOther(Element element) {
		if (Objects.equals(getSource(), element))
			return getTarget();
		else
			return getSource();
	}

	@Override
	default String getCategory() {
		return "Relationship";
	}

	default String getTextRepresentation() {
		return getSource().getTitle() + " (" + getSource().getType() + ") " + (isBidirectional() ? "<-- " : "-- ")
				+ getRelationshipType().getLabel().toUpperCase() + " -->" + " " + getTarget().getTitle() + " ("
				+ getTarget().getType() + ") ";
	}

	default String getLabel() {
		StringBuilder labelBuilder = new StringBuilder();
		labelBuilder.append(getTitle());
		if (!getTechnologies().isEmpty())
			labelBuilder.append(" [").append(getTechnologyLabel()).append("]");
		return labelBuilder.toString();
	}

	default String getTechnologyLabel() {
		return getTechnologies().stream().map(Concept::getTitle).collect(Collectors.joining(", "));
	}
}
