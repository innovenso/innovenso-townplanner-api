package com.innovenso.townplan.api.value.view;

import com.innovenso.townplan.api.value.Relationship;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface FlowView extends View {
	List<Step> getSteps();

	@Override
	default String getCategory() {
		return "flow";
	}

	default boolean isShowStepCounter() {
		return true;
	}

	@Override
	default Set<Relationship> getRelationships() {
		return getSteps().stream().map(Step::getRelationship).collect(Collectors.toSet());
	}
}
