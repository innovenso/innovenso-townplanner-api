package com.innovenso.townplan.api.value.view;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "StepKeyValuePairBuilder")
@JsonDeserialize(builder = StepKeyValuePair.StepKeyValuePairBuilder.class)
public class StepKeyValuePair {
	@NonNull
	String relationship;
	private String title;
	private boolean response;

	@JsonPOJOBuilder(withPrefix = "")
	public static class StepKeyValuePairBuilder {
	}
}
