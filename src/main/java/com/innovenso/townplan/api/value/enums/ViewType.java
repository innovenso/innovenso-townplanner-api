package com.innovenso.townplan.api.value.enums;

public enum ViewType {
	TOWN_PLAN, SEQUENCE, FLOW;
}
