package com.innovenso.townplan.api.value.aspects;

public interface Lifecycle extends ModelAspect {
	String getDescription();

	LifecyleType getType();
}
