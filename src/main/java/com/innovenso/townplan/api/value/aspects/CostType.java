package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum CostType {
	CAPEX("Capex"), OPEX("Opex"), UNKNOWN("Unknown");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	CostType(String label) {
		this.label = label;
	}

	public static Optional<CostType> getCostType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(CostType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
