package com.innovenso.townplan.api.value.aspects;

public interface Confidentiality extends ModelAspect {
	ConfidentialityLevel getLevel();
	String getDescription();
}
