package com.innovenso.townplan.api.value.aspects;

public interface ArchitectureVerdict extends ModelAspect {
	String getDescription();

	ArchitectureVerdictType getVerdictType();
}
