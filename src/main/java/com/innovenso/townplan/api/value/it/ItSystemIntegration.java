package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.enums.Criticality;
import com.innovenso.townplan.api.value.view.Step;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;

public interface ItSystemIntegration extends ActiveStructureElement {
	@Override
	default String getType() {
		return "Integration";
	}

	ItSystem getSourceSystem();

	ItSystem getTargetSystem();

	Optional<String> getVolume();

	Optional<String> getFrequency();

	Optional<Criticality> getCriticality();

	Optional<String> getCriticalityDescription();

	Optional<String> getResilience();

	default Set<ItSystem> getSystems() {
		return Objects.equals(getSourceSystem().getKey(), getTargetSystem().getKey())
				? Set.of(getSourceSystem())
				: Set.of(getSourceSystem(), getTargetSystem());
	}

	default ItSystem getOther(@NonNull ItSystem system) {
		return Objects.equals(getSourceSystem().getKey(), system.getKey()) ? getTargetSystem() : getSourceSystem();
	}

	default Set<ItSystem> getImplementingSystems() {
		return getIncomingRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.REALIZATION))
				.map(Relationship::getSource).filter(ItSystem.class::isInstance).map(ItSystem.class::cast)
				.collect(Collectors.toSet());
	}

	default Set<DataEntity> getDataEntities() {
		return getIncomingRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.COMPOSITION))
				.map(Relationship::getSource).filter(DataEntity.class::isInstance).map(DataEntity.class::cast)
				.collect(Collectors.toSet());
	}

	default String getTechnologyDescription() {
		if (getImplementingSystems() == null || getImplementingSystems().isEmpty())
			return "";
		else
			return getImplementingSystems().stream().map(Element::getTitle).collect(Collectors.joining(", "));
	}

	default Set<Relationship> getIntegrationRelationships() {
		return getSteps().stream().map(Step::getRelationship).collect(Collectors.toSet());
	}

	default String getCriticalityLabel() {
		return getCriticality().orElse(Criticality.UNKNOWN).getLabel();
	}

	List<Step> getSteps();
}
