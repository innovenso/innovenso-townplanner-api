package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import java.util.Set;

public interface HasImpact {
	Set<BusinessCapability> getImpactedBusinessCapabilities();

	Set<BusinessCapability> getAddedBusinessCapabilities();

	Set<BusinessCapability> getChangedBusinessCapabilities();

	Set<BusinessCapability> getRemovedBusinessCapabilities();

	Set<ItSystem> getImpactedItSystems();

	Set<ItSystem> getAddedItSystems();

	Set<ItSystem> getChangedItSystems();

	Set<ItSystem> getRemovedItSystems();

	Set<ItSystemIntegration> getImpactedItSystemIntegrations();

	Set<ItSystemIntegration> getAddedItSystemIntegrations();

	Set<ItSystemIntegration> getChangedItSystemIntegrations();

	Set<ItSystemIntegration> getRemovedItSystemIntegrations();

	Set<ItContainer> getImpactedItContainers();

	Set<ItContainer> getAddedItContainers();

	Set<ItContainer> getChangedItContainers();

	Set<ItContainer> getRemovedItContainers();

	Set<ArchitectureBuildingBlock> getImpactedBuildingBlocks();

	Set<ArchitectureBuildingBlock> getAddedBuildingBlocks();

	Set<ArchitectureBuildingBlock> getChangedBuildingBlocks();

	Set<ArchitectureBuildingBlock> getRemovedBuildingBlocks();

	Set<DataEntity> getImpactedDataEntities();

	Set<DataEntity> getAddedDataEntities();

	Set<DataEntity> getChangedDataEntities();

	Set<DataEntity> getRemovedDataEntities();
}
