package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ExternalIdType {
	SPARX("Sparx Enterprise Architect"), CONFLUENCE("Confluence Page ID");

	public final String label;

	public String getLabel() {
		return label;
	}

	ExternalIdType(String label) {
		this.label = label;
	}

	public static Optional<ExternalIdType> getExternalIdType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ExternalIdType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public String getValue() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
