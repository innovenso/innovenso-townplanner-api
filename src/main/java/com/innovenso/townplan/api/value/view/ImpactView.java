package com.innovenso.townplan.api.value.view;

import java.util.List;
import java.util.Optional;

public interface ImpactView extends View {
	@Override
	default String getCategory() {
		return "impact";
	}

	List<ImpactBusinessCapability> getCapabilities();

	Optional<ImpactBusinessCapability> getCapability(String key);
}
