package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ConfidentialityLevel {
	RED("Red", "personal for named recipients only"), AMBER("Amber",
			"limited distribution, on a need-to-know basis only"), GREEN("Green",
					"community-wide distribution, but not to be published on the internet or outside of the community"), WHITE(
							"White", "unlimited distribution, but subject to standard copyright rules");

	public final String label;
	public final String description;

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	ConfidentialityLevel(String label, String description) {
		this.label = label;
		this.description = description;
	}

	public static Optional<ConfidentialityLevel> getConfidentialityLevel(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ConfidentialityLevel.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public String getValue() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
