package com.innovenso.townplan.api.value.aspects;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum FatherTimeType {
	CONCEIVED("Conception",
			"The idea has been formed, but it exists only in the heads of people at this point."), EXPECTED("Due Date",
					"Mostly useful for decisions, projects and project milestones."), BIRTH("Birth",
							"Development has started."), GROWING("Pre-production",
									"There is at least a deliverable, typically in test or staging environments."), ACTIVE(
											"Production",
											"In production, from this point the concept will appear in diagrams."), RETIRED(
													"Retirement",
													"Phasing out. The concept is still in production, but ready to be decommissioned/replaced."), LIFE_EVENT(
															"Life event",
															"Any other event that deserves documentation."), DEATH(
																	"Death",
																	"The concept has been decommissioned, it will not appear in diagrams anymore after this point.");

	public final String label;
	public final String description;

	public String getLabel() {
		return label;
	}

	public String getLabelWithDescription() {
		return label.toUpperCase() + " - " + description;
	}

	public String getValue() {
		return name();
	}

	FatherTimeType(String label, String description) {
		this.label = label;
		this.description = description;
	}

	public static Optional<FatherTimeType> getFatherTimeType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(FatherTimeType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
