package com.innovenso.townplan.api.value.aspects;

public interface Cost extends ModelAspect {
	String getDescription();

	String getTitle();

	String getCategory();

	CostType getCostType();

	String getUnitOfMeasure();

	Double getNumberOfUnits();

	Double getCostPerUnit();

	Integer getFiscalYear();

	default Double getTotalCost() {
		return getCostPerUnit() * getNumberOfUnits();
	}
}
