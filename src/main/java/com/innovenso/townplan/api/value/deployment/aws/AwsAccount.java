package com.innovenso.townplan.api.value.deployment.aws;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.stream.Collectors;

public interface AwsAccount extends AwsElement {
	Set<AwsVpc> getVpcs();

	String getAccountId();

	Set<AwsInstance> getInstances();

	default Set<AwsRegion> getRegions() {
		return Sets.union(getVpcs().stream().map(AwsVpc::getRegion).collect(Collectors.toSet()),
				getInstances().stream().filter(instance -> instance.getRegion().isPresent())
						.map(instance -> instance.getRegion().get()).collect(Collectors.toSet()));
	}
}
