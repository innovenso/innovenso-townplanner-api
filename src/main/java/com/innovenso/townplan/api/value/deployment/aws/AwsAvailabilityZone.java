package com.innovenso.townplan.api.value.deployment.aws;

public interface AwsAvailabilityZone extends AwsElement {
	AwsRegion getRegion();
}
