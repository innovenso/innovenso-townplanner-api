package com.innovenso.townplan.api.value.it.decision;

import com.innovenso.townplan.api.value.PassiveStructureElement;

public interface DecisionContext extends PassiveStructureElement {
	Decision getDecision();

	String getSortKey();

	DecisionContextType getContextType();

	@Override
	default String getType() {
		return "Decision Context";
	}

	@Override
	default String getCategory() {
		return "Motivation";
	}
}
