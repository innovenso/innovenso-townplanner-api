package com.innovenso.townplan.api.value.aspects;

public interface Documentation extends ModelAspect {
	String getDescription();

	DocumentationType getType();

	String getSortKey();
}
