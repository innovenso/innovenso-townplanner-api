package com.innovenso.townplan.api.value.business;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.enums.ActorType;
import com.innovenso.townplan.api.value.it.ItSystem;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface BusinessActor extends ActiveStructureElement {
	default String getCategory() {
		return "Business Actor";
	}

	Enterprise getEnterprise();

	@Override
	default String getType() {
		return getActorType().getLabel();
	}

	ActorType getActorType();

	default boolean isPerson() {
		return getActorType().equals(ActorType.INDIVIDUAL);
	}

	default boolean isSquad() {
		return getActorType().equals(ActorType.TEAM)
				&& members().allMatch(actor -> actor.getActorType().equals(ActorType.INDIVIDUAL));
	}

	default boolean isTribe() {
		return getActorType().equals(ActorType.TEAM) && members().anyMatch(BusinessActor::isSquad);
	}

	default boolean isAlliance() {
		return getActorType().equals(ActorType.TEAM) && members().anyMatch(BusinessActor::isTribe);
	}

	default boolean hasMembers() {
		return members().findAny().isPresent();
	}

	default Stream<BusinessActor> members() {
		return getDirectIncomingDependencies().stream().filter(BusinessActor.class::isInstance)
				.map(BusinessActor.class::cast);
	}

	default Set<BusinessActor> getMembers() {
		return members().collect(Collectors.toSet());
	}

	default Set<ItSystem> getSystems() {
		return getDirectOutgoingDependencies().stream().filter(ItSystem.class::isInstance).map(ItSystem.class::cast)
				.collect(Collectors.toSet());
	}

	default Set<ItSystem> getOwnedSystems() {
		return getOutgoingRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.DELIVERY))
				.map(Relationship::getTarget).filter(ItSystem.class::isInstance).map(ItSystem.class::cast)
				.collect(Collectors.toSet());
	}
}
