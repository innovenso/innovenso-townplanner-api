package com.innovenso.townplan.api.value.aspects;

import java.util.Optional;

public interface ConstraintScore extends ModelAspect {
	String getDescription();

	Constraint getConstraint();

	ScoreWeight getWeight();

	@Override
	default Optional<ModelAspect> getParent() {
		return Optional.of(getConstraint());
	}
}
