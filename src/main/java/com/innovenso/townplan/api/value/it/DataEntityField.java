package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.PassiveStructureElement;

public interface DataEntityField extends PassiveStructureElement {
	DataEntity getDataEntity();

	boolean isMandatory();

	boolean isUnique();

	String getFieldConstraints();

	String getDefaultValue();

	String getLocalization();
}
