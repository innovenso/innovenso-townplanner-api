package com.innovenso.townplan.api.value.it.decision;

import com.innovenso.townplan.api.value.PassiveStructureElement;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.HasImpact;
import java.util.List;

public interface Decision extends PassiveStructureElement, HasImpact {
	Enterprise getEnterprise();

	DecisionStatus getStatus();

	String getOutcome();

	List<DecisionContext> getContexts();

	List<DecisionOption> getOptions();

	@Override
	default String getType() {
		return "Decision";
	}

	@Override
	default String getCategory() {
		return "Motivation";
	}
}
