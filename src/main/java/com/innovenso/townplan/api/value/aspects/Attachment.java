package com.innovenso.townplan.api.value.aspects;

public interface Attachment extends ModelAspect {
	String getCdnContentUrl();

	String getTitle();

	String getDescription();
}
