package com.innovenso.townplan.api.value.it;

import com.innovenso.townplan.api.value.ActiveStructureElement;
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation;
import com.innovenso.townplan.api.value.enums.TechnologyType;

public interface Technology extends ActiveStructureElement {
	public static final String UNKNOWN = "unknown";

	TechnologyRecommendation getRecommendation();

	TechnologyType getTechnologyType();

	@Override
	default String getCategory() {
		return "IT Architecture";
	}
}
