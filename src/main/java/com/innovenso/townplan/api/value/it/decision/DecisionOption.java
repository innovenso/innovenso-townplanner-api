package com.innovenso.townplan.api.value.it.decision;

import com.innovenso.townplan.api.value.PassiveStructureElement;

public interface DecisionOption extends PassiveStructureElement {
	Decision getDecision();

	String getSortKey();

	DecisionOptionVerdict getVerdict();

	@Override
	default String getType() {
		return "Decision Option";
	}

	@Override
	default String getCategory() {
		return "Motivation";
	}
}
