package com.innovenso.townplan.api.value.it.decision;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum DecisionOptionVerdict {
	UNDER_INVESTIGATION("Under Investigation"), CHOSEN("Chosen"), REJECTED("Rejected");

	public final String label;

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return name();
	}

	DecisionOptionVerdict(String label) {
		this.label = label;
	}

	public static Optional<DecisionOptionVerdict> getDecisionOptionVerdict(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(DecisionOptionVerdict.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
