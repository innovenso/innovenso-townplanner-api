package com.innovenso.townplan.api;

import com.innovenso.eventsourcing.api.entity.Entity;
import com.innovenso.townplan.api.value.*;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.ImpactView;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TownPlan extends Entity {
	<T extends Element> Set<T> getElements(Class<T> elementClass);

	<T extends Element> Optional<T> getElement(Class<T> elementClass, String key);

	Set<Relationship> getAllRelationships();

	Set<? extends Concept> getAllConcepts();

	Set<? extends ModelComponent> getAllModelComponents();

	Optional<? extends Concept> getConcept(String key);

	Optional<? extends ModelComponent> getModelComponent(String key);

	Optional<Relationship> getRelationship(String sourceKey, String targetKey, RelationshipType type);

	Optional<Relationship> getRelationship(Element source, Element target, RelationshipType type);

	Optional<Relationship> getRelationship(String key);

	boolean hasRelationship(String key);

	Set<FlowView> getFlowViews();

	Optional<FlowView> getFlowView(String key);

	Set<ImpactView> getImpactViews();

	Optional<ImpactView> getImpactView(String key);

	List<KeyPointInTime> getKeyPointsInTime();
}
