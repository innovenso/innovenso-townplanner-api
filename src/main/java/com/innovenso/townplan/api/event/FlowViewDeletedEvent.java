package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FlowViewDeletedEventBuilder")
@JsonDeserialize(builder = FlowViewDeletedEvent.FlowViewDeletedEventBuilder.class)
public class FlowViewDeletedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;

	@JsonPOJOBuilder(withPrefix = "")
	public static class FlowViewDeletedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Flow View %s was removed", key);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
