package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FlowViewStepsClearedEventBuilder")
@JsonDeserialize(builder = FlowViewStepsClearedEvent.FlowViewStepsClearedEventBuilder.class)
public class FlowViewStepsClearedEvent implements ViewEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String view;

	@JsonPOJOBuilder(withPrefix = "")
	public static class FlowViewStepsClearedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Flow View %s was cleared", view);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(view);
	}
}
