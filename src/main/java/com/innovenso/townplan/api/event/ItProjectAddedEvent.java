package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ItProjectAddedEventBuilder")
@JsonDeserialize(builder = ItProjectAddedEvent.ItProjectAddedEventBuilder.class)
public class ItProjectAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String type;
	private final String enterprise;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ItProjectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT Project %s (%s) was added to enterprise %s", title, key, enterprise);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
