package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SystemIntegrationStepsSetEventBuilder")
@JsonDeserialize(builder = SystemIntegrationStepsSetEvent.SystemIntegrationStepsSetEventBuilder.class)
public class SystemIntegrationStepsSetEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final List<StepKeyValuePair> steps;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SystemIntegrationStepsSetEventBuilder {
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("event id: ").append(id);
		builder.append("\nview: ").append(key);
		builder.append("\nsteps:\n")
				.append(steps.stream().map(StepKeyValuePair::toString).collect(Collectors.joining("\n")));
		return builder.toString();
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("System Integration %s has updated steps. It now has %d steps", key, steps.size());
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
