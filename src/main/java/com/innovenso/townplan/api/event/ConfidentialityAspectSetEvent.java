package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.ConfidentialityLevel;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Set;

@Value
@Builder(builderClassName = "AspectEventBuilder")
@JsonDeserialize(builder = ConfidentialityAspectSetEvent.AspectEventBuilder.class)
public class ConfidentialityAspectSetEvent implements TownPlanEvent {
	@NonNull
	EventId id;
	@NonNull
	String conceptKey;
	@NonNull
	ConfidentialityLevel level;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AspectEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has updated its confidentiality to %s", conceptKey, level.label);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
