package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SwotAspectDeletedEventBuilder")
@JsonDeserialize(builder = SwotAspectDeletedEvent.SwotAspectDeletedEventBuilder.class)
public class SwotAspectDeletedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String swotKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SwotAspectDeletedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has lost a SWOT", conceptKey);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
