package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AwsAutoscalingGroupAddedEventBuilder")
@JsonDeserialize(builder = AwsAutoscalingGroupAddedEvent.AwsAutoscalingGroupAddedEventBuilder.class)
public class AwsAutoscalingGroupAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String vpc;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AwsAutoscalingGroupAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, vpc);
	}
}
