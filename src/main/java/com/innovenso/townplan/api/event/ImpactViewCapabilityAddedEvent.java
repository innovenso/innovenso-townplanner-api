package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ImpactViewCapabilityAddedCommandBuilder")
@JsonDeserialize(builder = ImpactViewCapabilityAddedEvent.ImpactViewCapabilityAddedCommandBuilder.class)
public class ImpactViewCapabilityAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String view;
	@NonNull
	private final String capability;
	private final boolean impacted;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ImpactViewCapabilityAddedCommandBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(view, capability);
	}
}
