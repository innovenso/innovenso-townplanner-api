package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.SecurityConcernType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SecurityConcernAspectAddedEventBuilder")
@JsonDeserialize(builder = SecurityConcernAspectAddedEvent.SecurityConcernAspectAddedEventBuilder.class)
public class SecurityConcernAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final SecurityConcernType concernType;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SecurityConcernAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Security concern added to %s: %s", modelComponentKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(modelComponentKey);
	}
}
