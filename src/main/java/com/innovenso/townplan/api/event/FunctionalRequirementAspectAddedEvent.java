package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import java.util.Set;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FunctionalRequirementAspectAddedEventBuilder")
@JsonDeserialize(builder = FunctionalRequirementAspectAddedEvent.FunctionalRequirementAspectAddedEventBuilder.class)
public class FunctionalRequirementAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String description;
	private final String sortKey;
	@Builder.Default
	@NonNull
	private final RequirementWeight weight = RequirementWeight.SHOULD_HAVE;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class FunctionalRequirementAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Functional requirement added to %s: %s", modelComponentKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(modelComponentKey);
	}
}
