package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.enums.AwsInstanceType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AwsInstanceAddedEventBuilder")
@JsonDeserialize(builder = AwsInstanceAddedEvent.AwsInstanceAddedEventBuilder.class)
public class AwsInstanceAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final AwsInstanceType instanceType;
	@NonNull
	private final String account;
	private final String vpc;
	private final String region;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AwsInstanceAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, vpc, region);
	}
}
