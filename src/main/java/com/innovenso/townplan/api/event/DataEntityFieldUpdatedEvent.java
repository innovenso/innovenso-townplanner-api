package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = DataEntityFieldUpdatedEvent.Builder.class)
public class DataEntityFieldUpdatedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	String sortKey;
	@NonNull
	String type;
	boolean mandatory;
	boolean unique;
	String fieldConstraints;
	String defaultValue;
	String localization;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class Builder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
