package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AttachmentAspectAddedEventBuilder")
@JsonDeserialize(builder = AttachmentAspectAddedEvent.AttachmentAspectAddedEventBuilder.class)
public class AttachmentAspectAddedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String key;
	@NonNull
	String conceptKey;
	@NonNull
	String cdnContentUrl;
	@NonNull
	String title;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AttachmentAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has a new attachment: %s", conceptKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
