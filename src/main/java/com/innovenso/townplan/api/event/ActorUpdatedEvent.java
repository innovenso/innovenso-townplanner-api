package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;

import com.innovenso.townplan.api.value.enums.ActorType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ActorUpdatedEventBuilder")
@JsonDeserialize(builder = ActorUpdatedEvent.ActorUpdatedEventBuilder.class)
public class ActorUpdatedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	ActorType type;
	String enterprise;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Actor %s (%s) was updated", title, key);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class ActorUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, enterprise);
	}
}
