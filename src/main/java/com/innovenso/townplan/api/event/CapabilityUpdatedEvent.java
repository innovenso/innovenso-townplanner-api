package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "CapabilityUpdatedEventBuilder")
@JsonDeserialize(builder = CapabilityUpdatedEvent.CapabilityUpdatedEventBuilder.class)
public class CapabilityUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String enterprise;
	private final String parent;
	private final String sortKey;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Business Capability %s (%s) was updated", title, key);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class CapabilityUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, enterprise, parent);
	}
}
