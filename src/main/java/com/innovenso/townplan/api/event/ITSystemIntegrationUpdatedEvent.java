package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.enums.Criticality;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ITSystemIntegrationUpdatedEventBuilder")
@JsonDeserialize(builder = ITSystemIntegrationUpdatedEvent.ITSystemIntegrationUpdatedEventBuilder.class)
public class ITSystemIntegrationUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String volume;
	private final String frequency;
	private final Criticality criticality;
	private final String criticalityDescription;
	private final String resilience;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ITSystemIntegrationUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT System Integration %s (%s) was updated", title, key);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
