package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.SecurityImpactLevel;
import com.innovenso.townplan.api.value.aspects.SecurityImpactType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SecurityImpactAspectAddedEventBuilder")
@JsonDeserialize(builder = SecurityImpactAspectAddedEvent.SecurityImpactAspectAddedEventBuilder.class)
public class SecurityImpactAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final SecurityImpactType impactType;
	private final String description;
	@NonNull
	private final SecurityImpactLevel impactLevel;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SecurityImpactAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Security impact added to %s: %s", modelComponentKey, impactType.getLabel());
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(modelComponentKey);
	}
}
