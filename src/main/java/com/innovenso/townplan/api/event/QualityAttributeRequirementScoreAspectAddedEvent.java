package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.ScoreWeight;
import java.util.Set;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "TheBuilder")
@JsonDeserialize(builder = QualityAttributeRequirementScoreAspectAddedEvent.TheBuilder.class)
public class QualityAttributeRequirementScoreAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	private final String description;
	@NonNull
	private final String qualityAttributeRequirementKey;
	@Builder.Default
	@NonNull
	private final ScoreWeight weight = ScoreWeight.UNKNOWN;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class TheBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return "";
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(modelComponentKey);
	}
}
