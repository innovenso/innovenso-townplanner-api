package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ItProjectMilestoneAddedEventBuilder")
@JsonDeserialize(builder = ItProjectMilestoneAddedEvent.ItProjectMilestoneAddedEventBuilder.class)
public class ItProjectMilestoneAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String project;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ItProjectMilestoneAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT Project %s has a new milestone: %s", project, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, project);
	}
}
