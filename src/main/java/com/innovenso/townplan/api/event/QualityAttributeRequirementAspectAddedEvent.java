package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import java.util.Set;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "QualityAttributeRequirementAspectAddedEventBuilder")
@JsonDeserialize(builder = QualityAttributeRequirementAspectAddedEvent.QualityAttributeRequirementAspectAddedEventBuilder.class)
public class QualityAttributeRequirementAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String sourceOfStimulus;
	private final String stimulus;
	private final String environment;
	private final String response;
	private final String responseMeasure;
	private final String sortKey;
	@Builder.Default
	@NonNull
	private final RequirementWeight weight = RequirementWeight.SHOULD_HAVE;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class QualityAttributeRequirementAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Quality attribute requirement added to %s: %s", modelComponentKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(modelComponentKey);
	}
}
