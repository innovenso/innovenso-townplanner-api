package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DecisionContextUpdatedEventBuilder")
@JsonDeserialize(builder = DecisionContextUpdatedEvent.DecisionContextUpdatedEventBuilder.class)
public class DecisionContextUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String sortKey;
	@NonNull
	DecisionContextType contextType;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Decision Context %s (%s) was updated", title, key);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class DecisionContextUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
