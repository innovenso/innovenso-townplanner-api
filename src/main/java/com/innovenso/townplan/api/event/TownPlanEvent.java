package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import java.util.Set;

public interface TownPlanEvent extends EntityEvent {
	@JsonIgnore
	default String getStatusDescription() {
		return "Something undefined happened in the town plan";
	}

	Set<String> getImpactedModelComponentKeys();
}
