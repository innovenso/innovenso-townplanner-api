package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.DocumentationType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DocumentationAspectAddedEventBuilder")
@JsonDeserialize(builder = DocumentationAspectAddedEvent.DocumentationAspectAddedEventBuilder.class)
public class DocumentationAspectAddedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String conceptKey;
	@NonNull
	DocumentationType type;
	String description;
	String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DocumentationAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Documentation added to concept %s", conceptKey);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
