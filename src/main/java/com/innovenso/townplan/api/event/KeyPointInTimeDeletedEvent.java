package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.time.LocalDate;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "KeyPointInTimeDeletedEventBuilder")
@JsonDeserialize(builder = KeyPointInTimeDeletedEvent.KeyPointInTimeDeletedEventBuilder.class)
public class KeyPointInTimeDeletedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final LocalDate date;

	@JsonPOJOBuilder(withPrefix = "")
	public static class KeyPointInTimeDeletedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Key point in time removed from town plan: %s", date);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of();
	}
}
