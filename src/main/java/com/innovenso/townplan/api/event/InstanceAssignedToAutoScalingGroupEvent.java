package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "InstanceAssignedToAutoScalingGroupEventBuilder")
@JsonDeserialize(builder = InstanceAssignedToAutoScalingGroupEvent.InstanceAssignedToAutoScalingGroupEventBuilder.class)
public class InstanceAssignedToAutoScalingGroupEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String instance;
	@NonNull
	private final String autoscalingGroup;

	@JsonPOJOBuilder(withPrefix = "")
	public static class InstanceAssignedToAutoScalingGroupEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(instance, autoscalingGroup);
	}
}
