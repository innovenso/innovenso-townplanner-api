package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AwsElementClearedEventBuilder")
@JsonDeserialize(builder = AwsElementClearedEvent.AwsElementClearedEventBuilder.class)
public class AwsElementClearedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AwsElementClearedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of();
	}
}
