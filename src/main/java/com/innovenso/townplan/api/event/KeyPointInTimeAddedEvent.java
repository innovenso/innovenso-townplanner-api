package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.time.LocalDate;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "KeyPointInTimeAddedEventBuilder")
@JsonDeserialize(builder = KeyPointInTimeAddedEvent.KeyPointInTimeAddedEventBuilder.class)
public class KeyPointInTimeAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final LocalDate date;
	private final String title;
	private boolean diagramsNeeded;

	@JsonPOJOBuilder(withPrefix = "")
	public static class KeyPointInTimeAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Key point in time added to town plan: %s", date);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of();
	}
}
