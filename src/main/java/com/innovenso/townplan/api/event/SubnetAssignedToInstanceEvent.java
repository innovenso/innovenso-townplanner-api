package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SubnetAssignedToInstanceEventBuilder")
@JsonDeserialize(builder = SubnetAssignedToInstanceEvent.SubnetAssignedToInstanceEventBuilder.class)
public class SubnetAssignedToInstanceEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String subnetKey;
	@NonNull
	private final String instanceKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SubnetAssignedToInstanceEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(instanceKey, subnetKey);
	}
}
