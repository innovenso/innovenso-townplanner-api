package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DecisionContextAddedEventBuilder")
@JsonDeserialize(builder = DecisionContextAddedEvent.DecisionContextAddedEventBuilder.class)
public class DecisionContextAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String decision;
	@NonNull
	DecisionContextType contextType;
	private final String description;
	@NonNull
	private final String sortKey;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Context %s (%s) was added to decision %s", title, key, decision);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class DecisionContextAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, decision);
	}
}
