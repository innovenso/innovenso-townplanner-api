package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "TechnologyAddedEventBuilder")
@JsonDeserialize(builder = TechnologyAddedEvent.TechnologyAddedEventBuilder.class)
public class TechnologyAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String type;
	private final TechnologyRecommendation recommendation;
	private final TechnologyType technologyType;

	@JsonPOJOBuilder(withPrefix = "")
	public static class TechnologyAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Technology %s (%s) was added with architecture recommendation %s", title, key,
				recommendation);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
