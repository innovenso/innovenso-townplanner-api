package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "BuildingBlockUpdatedEventBuilder")
@JsonDeserialize(builder = BuildingBlockUpdatedEvent.BuildingBlockUpdatedEventBuilder.class)
public class BuildingBlockUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String enterprise;
	private final String description;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Architecture Building Block %s (%s) was updated", title, key, enterprise);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class BuildingBlockUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, enterprise);
	}
}
