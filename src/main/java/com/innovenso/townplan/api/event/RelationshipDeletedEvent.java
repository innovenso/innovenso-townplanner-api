package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.RelationshipType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "RelationshipDeletedEventBuilder")
@JsonDeserialize(builder = RelationshipDeletedEvent.RelationshipDeletedEventBuilder.class)
public class RelationshipDeletedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String source;
	@NonNull
	private final String target;
	@NonNull
	private final RelationshipType type;

	@JsonPOJOBuilder(withPrefix = "")
	public static class RelationshipDeletedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Relationship if type %s removed from %s to %s", type.label, source, target);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(source, target);
	}
}
