package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;

import com.innovenso.townplan.api.value.enums.ContainerType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ITContainerAddedEventBuilder")
@JsonDeserialize(builder = ITContainerAddedEvent.ITContainerAddedEventBuilder.class)
public class ITContainerAddedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	String system;
	@NonNull
	ContainerType type;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ITContainerAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT Container %s (%s) was added to system %s", title, key, system);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, system);
	}
}
