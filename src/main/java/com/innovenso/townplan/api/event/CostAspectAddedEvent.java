package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.CostType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "CostAspectAddedEventBuilder")
@JsonDeserialize(builder = CostAspectAddedEvent.CostAspectAddedEventBuilder.class)
public class CostAspectAddedEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String conceptKey;
	private final String description;
	@NonNull
	private final String title;
	@NonNull
	private final String category;
	@NonNull
	private final CostType costType;
	@NonNull
	private final String unitOfMeasure;
	@NonNull
	private final Double numberOfUnits;
	@NonNull
	private final Double costPerUnit;
	@NonNull
	private final Integer fiscalYear;

	@JsonPOJOBuilder(withPrefix = "")
	public static class CostAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has a new cost %s", conceptKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
