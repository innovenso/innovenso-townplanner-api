package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "RelationshipTechnologyAddedEventBuilder")
@JsonDeserialize(builder = RelationshipTechnologyAddedEvent.RelationshipTechnologyAddedEventBuilder.class)
public class RelationshipTechnologyAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String relationship;
	@NonNull
	private final String technology;

	@JsonPOJOBuilder(withPrefix = "")
	public static class RelationshipTechnologyAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Relationship %s has a new technology: %s", relationship, technology);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(relationship);
	}
}
