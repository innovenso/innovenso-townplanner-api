package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DocumentationAspectDeletedEventBuilder")
@JsonDeserialize(builder = DocumentationAspectDeletedEvent.DocumentationAspectDeletedEventBuilder.class)
public class DocumentationAspectDeletedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String documentationKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DocumentationAspectDeletedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Documentation removed from concept %s", conceptKey);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
