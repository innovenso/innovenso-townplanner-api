package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AvailabilityZoneAssignedToAutoScalingGroupEventBuilder")
@JsonDeserialize(builder = AvailabilityZoneAssignedToAutoScalingGroupEvent.AvailabilityZoneAssignedToAutoScalingGroupEventBuilder.class)
public class AvailabilityZoneAssignedToAutoScalingGroupEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String availabilityZone;
	@NonNull
	private final String autoScalingGroup;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AvailabilityZoneAssignedToAutoScalingGroupEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(availabilityZone, autoScalingGroup);
	}
}
