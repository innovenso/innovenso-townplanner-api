package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ITPlatformUpdatedEventBuilder")
@JsonDeserialize(builder = ITPlatformUpdatedEvent.ITPlatformUpdatedEventBuilder.class)
public class ITPlatformUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ITPlatformUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT Platform %s (%s) was updated", title, key);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
