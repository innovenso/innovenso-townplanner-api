package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SecurityGroupAssignedToInstanceEventBuilder")
@JsonDeserialize(builder = SecurityGroupAssignedToInstanceEvent.SecurityGroupAssignedToInstanceEventBuilder.class)
public class SecurityGroupAssignedToInstanceEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String securityGroupKey;
	@NonNull
	private final String instanceKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SecurityGroupAssignedToInstanceEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(securityGroupKey, instanceKey);
	}
}
