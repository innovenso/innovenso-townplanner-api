package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FlowViewStepsSetEventBuilder")
@JsonDeserialize(builder = FlowViewStepsSetEvent.FlowViewStepsSetEventBuilder.class)
public class FlowViewStepsSetEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String view;
	@NonNull
	private final List<StepKeyValuePair> steps;

	@JsonPOJOBuilder(withPrefix = "")
	public static class FlowViewStepsSetEventBuilder {
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("event id: ").append(id);
		builder.append("\nview: ").append(view);
		builder.append("\nsteps:\n")
				.append(steps.stream().map(StepKeyValuePair::toString).collect(Collectors.joining("\n")));
		return builder.toString();
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Flow View %s has updated steps. View now has %d steps", view, steps.size());
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(view);
	}
}
