package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FlowViewStepAddedEventBuilder")
@JsonDeserialize(builder = FlowViewStepAddedEvent.FlowViewStepAddedEventBuilder.class)
public class FlowViewStepAddedEvent implements ViewEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String view;
	@NonNull
	private final String title;
	private final boolean response;
	private final String relationship;

	@JsonPOJOBuilder(withPrefix = "")
	public static class FlowViewStepAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Flow View %s has a new step: %s", view, relationship);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(view);
	}
}
