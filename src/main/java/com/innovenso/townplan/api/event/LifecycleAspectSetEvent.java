package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.LifecyleType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "LifecycleAspectSetEventBuilder")
@JsonDeserialize(builder = LifecycleAspectSetEvent.LifecycleAspectSetEventBuilder.class)
public class LifecycleAspectSetEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String conceptKey;
	@NonNull
	private final LifecyleType type;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class LifecycleAspectSetEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has updated its lifecycle to %s", conceptKey, type.label);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
