package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SubnetAssignedToAutoScalingGroupEventBuilder")
@JsonDeserialize(builder = SubnetAssignedToAutoScalingGroupEvent.SubnetAssignedToAutoScalingGroupEventBuilder.class)
public class SubnetAssignedToAutoScalingGroupEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String subnet;
	@NonNull
	private final String autoScalingGroup;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SubnetAssignedToAutoScalingGroupEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(subnet, autoScalingGroup);
	}
}
