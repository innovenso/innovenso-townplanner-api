package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.RelationshipType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "RelationshipAddedEventBuilder")
@JsonDeserialize(builder = RelationshipAddedEvent.RelationshipAddedEventBuilder.class)
public class RelationshipAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String source;
	@NonNull
	private final String title;
	@NonNull
	private final String target;
	@NonNull
	private final RelationshipType type;
	private final boolean bidirectional;
	private final String description;
	private final String integrationIdentifier;
	@NonNull
	@Builder.Default
	private final Integer influence = 100;
	@NonNull
	@Builder.Default
	private final Integer interest = 100;

	@JsonPOJOBuilder(withPrefix = "")
	public static class RelationshipAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Relationship if type %s added from %s to %s: %s", type.label, source, target, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(source, target, integrationIdentifier);
	}
}
