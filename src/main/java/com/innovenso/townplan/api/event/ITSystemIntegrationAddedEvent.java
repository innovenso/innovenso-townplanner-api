package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.enums.Criticality;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ITSystemIntegrationAddedEventBuilder")
@JsonDeserialize(builder = ITSystemIntegrationAddedEvent.ITSystemIntegrationAddedEventBuilder.class)
public class ITSystemIntegrationAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String sourceSystem;
	@NonNull
	private final String targetSystem;
	private final String description;
	private final String volume;
	private final String frequency;
	private final Criticality criticality;
	private final String criticalityDescription;
	private final String resilience;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ITSystemIntegrationAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT System Integration %s (%s) was added between systems %s and %s", title, key,
				sourceSystem, targetSystem);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, sourceSystem, targetSystem);
	}
}
