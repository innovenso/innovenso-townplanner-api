package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "PrincipleAddedEventBuilder")
@JsonDeserialize(builder = PrincipleAddedEvent.PrincipleAddedEventBuilder.class)
public class PrincipleAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String type;
	private final String enterprise;
	private final String sortKey;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Principle %s (%s) was added to enterprise %s", title, key, enterprise);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class PrincipleAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, enterprise);
	}
}
