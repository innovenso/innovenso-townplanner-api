package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ItProjectMilestoneUpdatedEventBuilder")
@JsonDeserialize(builder = ItProjectMilestoneUpdatedEvent.ItProjectMilestoneUpdatedEventBuilder.class)
public class ItProjectMilestoneUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String project;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ItProjectMilestoneUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("IT Project %s has an updated milestone: %s", project, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, project);
	}
}
