package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.enums.EntityType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DataEntityUpdatedEventBuilder")
@JsonDeserialize(builder = DataEntityUpdatedEvent.DataEntityUpdatedEventBuilder.class)
public class DataEntityUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String masterSystem;
	private final boolean consentNeeded;
	private final String sensitivity;
	private final String commercialValue;
	private final String enterprise;
	private final String businessCapability;
	@NonNull
	private final EntityType entityType;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Data Entity %s (%s) was updated", title, key);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class DataEntityUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, masterSystem, enterprise, businessCapability);
	}
}
