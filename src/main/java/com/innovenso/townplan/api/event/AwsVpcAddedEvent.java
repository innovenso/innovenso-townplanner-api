package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AwsVpcAddedEventBuilder")
@JsonDeserialize(builder = AwsVpcAddedEvent.AwsVpcAddedEventBuilder.class)
public class AwsVpcAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String region;
	@NonNull
	private final String networkMask;
	@NonNull
	private final String account;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AwsVpcAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
