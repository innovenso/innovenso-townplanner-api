package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.FatherTimeType;
import java.time.LocalDate;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "FatherTimeAspectAddedEventBuilder")
@JsonDeserialize(builder = FatherTimeAspectAddedEvent.FatherTimeAspectAddedEventBuilder.class)
public class FatherTimeAspectAddedEvent implements ElementEvent {
	@NonNull
	EventId id;
	@NonNull
	String key;
	@NonNull
	String conceptKey;
	@NonNull
	FatherTimeType fatherTimeType;
	@NonNull
	LocalDate pointInTime;
	@NonNull
	String title;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class FatherTimeAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has a new father time aspect: %s", conceptKey, title);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
