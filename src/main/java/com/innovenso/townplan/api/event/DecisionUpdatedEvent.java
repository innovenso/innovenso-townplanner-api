package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.it.decision.DecisionStatus;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DecisionUpdatedEventBuilder")
@JsonDeserialize(builder = DecisionUpdatedEvent.DecisionUpdatedEventBuilder.class)
public class DecisionUpdatedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final DecisionStatus status;
	private final String description;
	private final String outcome;

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Decision %s (%s) was updated", title, key);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class DecisionUpdatedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key);
	}
}
