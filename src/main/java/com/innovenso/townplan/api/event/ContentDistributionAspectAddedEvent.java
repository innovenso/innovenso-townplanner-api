package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ContentDistributionAspectAddedEventBuilder")
@JsonDeserialize(builder = ContentDistributionAspectAddedEvent.ContentDistributionAspectAddedEventBuilder.class)
public class ContentDistributionAspectAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final ContentOutputType type;
	@NonNull
	private final String url;
	private String title;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ContentDistributionAspectAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return "";
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of();
	}
}
