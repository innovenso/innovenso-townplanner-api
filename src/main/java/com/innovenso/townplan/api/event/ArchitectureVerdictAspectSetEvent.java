package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ArchitectureVerdictAspectSetEventBuilder")
@JsonDeserialize(builder = ArchitectureVerdictAspectSetEvent.ArchitectureVerdictAspectSetEventBuilder.class)
public class ArchitectureVerdictAspectSetEvent implements TownPlanEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String conceptKey;
	@NonNull
	private final ArchitectureVerdictType verdictType;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ArchitectureVerdictAspectSetEventBuilder {
	}

	@Override
	@JsonIgnore
	public String getStatusDescription() {
		return String.format("Concept %s has updated its architecture verdict to %s", conceptKey, verdictType.label);
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(conceptKey);
	}
}
