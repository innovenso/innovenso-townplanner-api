package com.innovenso.townplan.api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AwsElasticIpAddressAddedEventBuilder")
@JsonDeserialize(builder = AwsElasticIpAddressAddedEvent.AwsElasticIpAddressAddedEventBuilder.class)
public class AwsElasticIpAddressAddedEvent implements ElementEvent {
	@NonNull
	private final EventId id;
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String region;
	@NonNull
	private final String account;
	private final String instance;
	private final String allocationId;
	private final String associationId;
	private final String carrierIp;
	private final String customerOwnedIp;
	private final String customerOwnedIpv4Pool;
	private final String domain;
	private final String instanceId;
	private final String networkBorderGroup;
	private final String networkInterfaceId;
	private final String networkInterfaceOwnerId;
	private final String privateIpAddress;
	private final String publicIpAddress;
	private final String publicIpv4Pool;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AwsElasticIpAddressAddedEventBuilder {
	}

	@Override
	@JsonIgnore
	public Set<String> getImpactedModelComponentKeys() {
		return Set.of(key, instance);
	}
}
