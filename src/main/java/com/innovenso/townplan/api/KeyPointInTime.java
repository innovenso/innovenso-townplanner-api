package com.innovenso.townplan.api;

import java.time.LocalDate;

public interface KeyPointInTime {
	LocalDate getDate();

	String getTitle();

	boolean isDiagramsNeeded();

	boolean isToday();

	boolean isFuture();

	boolean isPast();
}
