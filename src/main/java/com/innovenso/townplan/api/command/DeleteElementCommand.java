package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteElementCommandBuilder")
@JsonDeserialize(builder = DeleteElementCommand.DeleteElementCommandBuilder.class)
public class DeleteElementCommand implements ViewCommand {
	@NonNull
	private final String key;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteElementCommandBuilder {
	}
}
