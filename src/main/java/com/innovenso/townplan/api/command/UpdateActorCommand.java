package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.ActorType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateActorCommandBuilder")
@JsonDeserialize(builder = UpdateActorCommand.UpdateActorCommandBuilder.class)
public class UpdateActorCommand implements ElementCommand {
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	ActorType type;
	String enterprise;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateActorCommandBuilder {
	}
}
