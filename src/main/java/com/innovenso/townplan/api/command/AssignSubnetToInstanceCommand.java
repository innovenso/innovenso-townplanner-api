package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AssignSubnetToInstanceCommandBuilder")
@JsonDeserialize(builder = AssignSubnetToInstanceCommand.AssignSubnetToInstanceCommandBuilder.class)
public class AssignSubnetToInstanceCommand implements TownPlanCommand {
	@NonNull
	private final String subnetKey;
	@NonNull
	private final String instanceKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AssignSubnetToInstanceCommandBuilder {
	}
}
