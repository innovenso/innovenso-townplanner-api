package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.ActorType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddActorCommandBuilder")
@JsonDeserialize(builder = AddActorCommand.AddActorCommandBuilder.class)
public class AddActorCommand implements ElementCommand {
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	ActorType type;
	String enterprise;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddActorCommandBuilder {
	}
}
