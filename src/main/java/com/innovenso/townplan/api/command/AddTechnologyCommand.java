package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddTechnologyCommandBuilder")
@JsonDeserialize(builder = AddTechnologyCommand.AddTechnologyCommandBuilder.class)
public class AddTechnologyCommand implements ElementCommand {
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	String type;
	TechnologyRecommendation recommendation;
	TechnologyType technologyType;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddTechnologyCommandBuilder {
	}
}
