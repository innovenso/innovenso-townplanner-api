package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import java.util.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SetFlowViewStepsCommandBuilder")
@JsonDeserialize(builder = SetFlowViewGroupsCommand.SetFlowViewStepsCommandBuilder.class)
public class SetFlowViewGroupsCommand implements ViewCommand {
	@NonNull
	private final String view;
	@NonNull
	private final List<StepKeyValuePair> steps;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SetFlowViewStepsCommandBuilder {
	}
}
