package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateEnterpriseCommandBuilder")
@JsonDeserialize(builder = UpdateEnterpriseCommand.UpdateEnterpriseCommandBuilder.class)
public class UpdateEnterpriseCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateEnterpriseCommandBuilder {
	}
}
