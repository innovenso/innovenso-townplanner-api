package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.EntityType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddDataEntityCommandBuilder")
@JsonDeserialize(builder = AddDataEntityCommand.AddDataEntityCommandBuilder.class)
public class AddDataEntityCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String masterSystem;
	private final boolean consentNeeded;
	private final String sensitivity;
	private final String commercialValue;
	private final String enterprise;
	private final String businessCapability;
	@NonNull
	private final EntityType entityType;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddDataEntityCommandBuilder {
	}
}
