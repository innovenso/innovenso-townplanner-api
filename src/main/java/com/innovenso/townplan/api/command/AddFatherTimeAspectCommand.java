package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.FatherTimeType;
import java.time.LocalDate;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddFatherTimeAspectCommandBuilder")
@JsonDeserialize(builder = AddFatherTimeAspectCommand.AddFatherTimeAspectCommandBuilder.class)
public class AddFatherTimeAspectCommand implements TownPlanCommand {
	@NonNull
	String key;
	@NonNull
	String conceptKey;
	@NonNull
	FatherTimeType fatherTimeType;
	@NonNull
	LocalDate pointInTime;
	@NonNull
	String title;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddFatherTimeAspectCommandBuilder {
	}
}
