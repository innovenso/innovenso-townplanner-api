package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.DocumentationType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddDocumentationAspectCommandBuilder")
@JsonDeserialize(builder = AddDocumentationAspectCommand.AddDocumentationAspectCommandBuilder.class)
public class AddDocumentationAspectCommand implements TownPlanCommand {
	@NonNull
	String conceptKey;
	@NonNull
	DocumentationType type;
	String description;
	String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddDocumentationAspectCommandBuilder {
	}
}
