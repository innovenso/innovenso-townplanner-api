package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddContentDistributionAspectCommandBuilder")
@JsonDeserialize(builder = AddContentDistributionAspectCommand.AddContentDistributionAspectCommandBuilder.class)
public class AddContentDistributionAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final ContentOutputType type;
	@NonNull
	private final String url;
	private String title;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddContentDistributionAspectCommandBuilder {
	}
}
