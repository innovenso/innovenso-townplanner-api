package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AssignAvailabilityZoneToAutoScalingGroupCommandBuilder")
@JsonDeserialize(builder = AssignAvailabilityZoneToAutoScalingGroupCommand.AssignAvailabilityZoneToAutoScalingGroupCommandBuilder.class)
public class AssignAvailabilityZoneToAutoScalingGroupCommand implements ElementCommand {
	@NonNull
	private final String availabilityZone;
	@NonNull
	private final String autoScalingGroup;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AssignAvailabilityZoneToAutoScalingGroupCommandBuilder {
	}
}
