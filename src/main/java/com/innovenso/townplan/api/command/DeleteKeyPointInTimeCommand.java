package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.time.LocalDate;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteKeyPointInTimeCommandBuilder")
@JsonDeserialize(builder = DeleteKeyPointInTimeCommand.DeleteKeyPointInTimeCommandBuilder.class)
public class DeleteKeyPointInTimeCommand implements TownPlanCommand {
	@NonNull
	private final LocalDate date;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteKeyPointInTimeCommandBuilder {
	}
}
