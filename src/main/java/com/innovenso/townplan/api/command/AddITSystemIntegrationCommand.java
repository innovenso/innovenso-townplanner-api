package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.Criticality;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddITSystemIntegrationCommandBuilder")
@JsonDeserialize(builder = AddITSystemIntegrationCommand.AddITSystemIntegrationCommandBuilder.class)
public class AddITSystemIntegrationCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String sourceSystem;
	@NonNull
	private final String targetSystem;
	private final String description;
	private final String volume;
	private final String frequency;
	private final Criticality criticality;
	private final String criticalityDescription;
	private final String resilience;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddITSystemIntegrationCommandBuilder {
	}
}
