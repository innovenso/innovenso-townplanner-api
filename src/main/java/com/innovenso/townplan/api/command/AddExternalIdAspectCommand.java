package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ExternalIdType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddExternalIdAspectCommandBuilder")
@JsonDeserialize(builder = AddExternalIdAspectCommand.AddExternalIdAspectCommandBuilder.class)
public class AddExternalIdAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final ExternalIdType type;
	@NonNull
	private final String value;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddExternalIdAspectCommandBuilder {
	}
}
