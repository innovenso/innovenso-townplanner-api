package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddRelationshipTechnologyCommandBuilder")
@JsonDeserialize(builder = AddRelationshipTechnologyCommand.AddRelationshipTechnologyCommandBuilder.class)
public class AddRelationshipTechnologyCommand implements ElementCommand {
	@NonNull
	private final String relationship;
	@NonNull
	private final String technology;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddRelationshipTechnologyCommandBuilder {
	}
}
