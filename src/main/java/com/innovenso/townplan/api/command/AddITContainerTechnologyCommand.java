package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddITContainerTechnologyCommandBuilder")
@JsonDeserialize(builder = AddITContainerTechnologyCommand.AddITContainerTechnologyCommandBuilder.class)
public class AddITContainerTechnologyCommand implements ElementCommand {
	@NonNull
	private final String container;
	@NonNull
	private final String technology;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddITContainerTechnologyCommandBuilder {
	}
}
