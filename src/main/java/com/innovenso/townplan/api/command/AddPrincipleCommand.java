package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddPrincipleCommandBuilder")
@JsonDeserialize(builder = AddPrincipleCommand.AddPrincipleCommandBuilder.class)
public class AddPrincipleCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String type;
	private final String description;
	private final String enterprise;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddPrincipleCommandBuilder {
	}
}
