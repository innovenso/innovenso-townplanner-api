package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateFlowViewCommandBuilder")
@JsonDeserialize(builder = UpdateFlowViewCommand.UpdateFlowViewCommandBuilder.class)
public class UpdateFlowViewCommand implements ViewCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final boolean showStepCounter;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateFlowViewCommandBuilder {
	}
}
