package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsSecurityGroupCommandBuilder")
@JsonDeserialize(builder = AddAwsSecurityGroupCommand.AddAwsSecurityGroupCommandBuilder.class)
public class AddAwsSecurityGroupCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String vpc;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsSecurityGroupCommandBuilder {
	}
}
