package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ScoreWeight;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "TheBuilder")
@JsonDeserialize(builder = AddConstraintScoreAspectCommand.TheBuilder.class)
public class AddConstraintScoreAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	private final String description;
	@NonNull
	private final String constraintKey;
	@Builder.Default
	@NonNull
	private final ScoreWeight weight = ScoreWeight.UNKNOWN;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class TheBuilder {
	}
}
