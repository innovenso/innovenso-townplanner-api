package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.CostType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddCostAspectCommandBuilder")
@JsonDeserialize(builder = AddCostAspectCommand.AddCostAspectCommandBuilder.class)
public class AddCostAspectCommand implements TownPlanCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String conceptKey;
	private final String description;
	@NonNull
	private final String title;
	@NonNull
	private final String category;
	@NonNull
	private final CostType costType;
	@NonNull
	private final String unitOfMeasure;
	@NonNull
	private final Double numberOfUnits;
	@NonNull
	private final Double costPerUnit;
	@NonNull
	private final Integer fiscalYear;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddCostAspectCommandBuilder {
	}
}
