package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteFlowViewCommandBuilder")
@JsonDeserialize(builder = DeleteFlowViewCommand.DeleteFlowViewCommandBuilder.class)
public class DeleteFlowViewCommand implements ViewCommand {
	@NonNull
	private final String key;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteFlowViewCommandBuilder {
	}
}
