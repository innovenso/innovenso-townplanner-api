package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.it.decision.DecisionStatus;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateDecisionCommandBuilder")
@JsonDeserialize(builder = UpdateDecisionCommand.UpdateDecisionCommandBuilder.class)
public class UpdateDecisionCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final DecisionStatus status;
	private final String description;
	private final String outcome;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateDecisionCommandBuilder {
	}
}
