package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AssignInstanceToAutoScalingGroupCommandBuilder")
@JsonDeserialize(builder = AssignInstanceToAutoScalingGroupCommand.AssignInstanceToAutoScalingGroupCommandBuilder.class)
public class AssignInstanceToAutoScalingGroupCommand implements ElementCommand {
	@NonNull
	private final String instance;
	@NonNull
	private final String autoscalingGroup;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AssignInstanceToAutoScalingGroupCommandBuilder {
	}
}
