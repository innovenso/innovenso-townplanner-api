package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.RelationshipType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddRelationshipCommandBuilder")
@JsonDeserialize(builder = AddRelationshipCommand.AddRelationshipCommandBuilder.class)
public class AddRelationshipCommand implements ElementCommand {
	@NonNull
	private final String source;
	@NonNull
	private final String title;
	@NonNull
	private final String target;
	private final boolean bidirectional;
	@NonNull
	private final RelationshipType type;
	private final String description;
	private final String integrationIdentifier;
	@NonNull
	@Builder.Default
	private final Integer influence = 100;
	@NonNull
	@Builder.Default
	private final Integer interest = 100;

	@Override
	public String getKey() {
		return source + type.name() + target;
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddRelationshipCommandBuilder {
	}
}
