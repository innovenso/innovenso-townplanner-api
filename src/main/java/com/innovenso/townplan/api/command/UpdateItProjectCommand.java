package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateItProjectCommandBuilder")
@JsonDeserialize(builder = UpdateItProjectCommand.UpdateItProjectCommandBuilder.class)
public class UpdateItProjectCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String type;
	private final String enterprise;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateItProjectCommandBuilder {
	}
}
