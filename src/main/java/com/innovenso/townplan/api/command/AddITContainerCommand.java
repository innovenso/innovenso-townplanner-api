package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.ContainerType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddITContainerCommandBuilder")
@JsonDeserialize(builder = AddITContainerCommand.AddITContainerCommandBuilder.class)
public class AddITContainerCommand implements ElementCommand {
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	@NonNull
	String system;
	@NonNull
	ContainerType type;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddITContainerCommandBuilder {
	}
}
