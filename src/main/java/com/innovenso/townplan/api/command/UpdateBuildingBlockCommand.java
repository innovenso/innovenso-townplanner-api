package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateBuildingBlockCommandBuilder")
@JsonDeserialize(builder = UpdateBuildingBlockCommand.UpdateBuildingBlockCommandBuilder.class)
public class UpdateBuildingBlockCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String enterprise;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateBuildingBlockCommandBuilder {
	}
}
