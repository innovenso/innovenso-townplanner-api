package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.LifecyleType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SetLifecycleAspectCommandBuilder")
@JsonDeserialize(builder = SetLifecycleAspectCommand.SetLifecycleAspectCommandBuilder.class)
public class SetLifecycleAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final LifecyleType lifecyleType;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SetLifecycleAspectCommandBuilder {
	}
}
