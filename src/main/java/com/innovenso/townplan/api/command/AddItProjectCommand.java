package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddItProjectCommandBuilder")
@JsonDeserialize(builder = AddItProjectCommand.AddItProjectCommandBuilder.class)
public class AddItProjectCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String type;
	private final String enterprise;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddItProjectCommandBuilder {
	}
}
