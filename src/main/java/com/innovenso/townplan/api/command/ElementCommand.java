package com.innovenso.townplan.api.command;

public interface ElementCommand extends TownPlanCommand {
	String getKey();
}
