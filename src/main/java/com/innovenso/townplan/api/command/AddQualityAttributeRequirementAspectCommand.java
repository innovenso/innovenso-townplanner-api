package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddQualityAttributeRequirementAspectCommandBuilder")
@JsonDeserialize(builder = AddQualityAttributeRequirementAspectCommand.AddQualityAttributeRequirementAspectCommandBuilder.class)
public class AddQualityAttributeRequirementAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String sourceOfStimulus;
	private final String stimulus;
	private final String environment;
	private final String response;
	private final String responseMeasure;
	private final String sortKey;
	@Builder.Default
	private final RequirementWeight weight = RequirementWeight.SHOULD_HAVE;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddQualityAttributeRequirementAspectCommandBuilder {
	}
}
