package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteSwotAspectCommandBuilder")
@JsonDeserialize(builder = DeleteSwotAspectCommand.DeleteSwotAspectCommandBuilder.class)
public class DeleteSwotAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String swotKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteSwotAspectCommandBuilder {
	}
}
