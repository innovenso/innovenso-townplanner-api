package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.RelationshipType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteRelationshipCommandBuilder")
@JsonDeserialize(builder = DeleteRelationshipCommand.DeleteRelationshipCommandBuilder.class)
public class DeleteRelationshipCommand implements ElementCommand {
	@NonNull
	private final String source;
	@NonNull
	private final String target;
	@NonNull
	private final RelationshipType type;

	@Override
	public String getKey() {
		return source + type.name() + target;
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteRelationshipCommandBuilder {
	}
}
