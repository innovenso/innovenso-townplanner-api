package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateDecisionContextCommandBuilder")
@JsonDeserialize(builder = UpdateDecisionContextCommand.UpdateDecisionContextCommandBuilder.class)
public class UpdateDecisionContextCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String sortKey;
	@NonNull
	DecisionContextType contextType;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateDecisionContextCommandBuilder {
	}
}
