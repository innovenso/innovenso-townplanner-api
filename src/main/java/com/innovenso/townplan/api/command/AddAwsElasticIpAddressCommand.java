package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsElasticIpAddressCommandBuilder")
@JsonDeserialize(builder = AddAwsElasticIpAddressCommand.AddAwsElasticIpAddressCommandBuilder.class)
public class AddAwsElasticIpAddressCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String region;
	@NonNull
	private final String account;
	private final String instance;
	private final String allocationId;
	private final String associationId;
	private final String carrierIp;
	private final String customerOwnedIp;
	private final String customerOwnedIpv4Pool;
	private final String domain;
	private final String instanceId;
	private final String networkBorderGroup;
	private final String networkInterfaceId;
	private final String networkInterfaceOwnerId;
	private final String privateIpAddress;
	private final String publicIpAddress;
	private final String publicIpv4Pool;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsElasticIpAddressCommandBuilder {
	}
}
