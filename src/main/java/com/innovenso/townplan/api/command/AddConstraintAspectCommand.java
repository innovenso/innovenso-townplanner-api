package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import java.util.UUID;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddConstraintAspectCommandBuilder")
@JsonDeserialize(builder = AddConstraintAspectCommand.AddConstraintAspectCommandBuilder.class)
public class AddConstraintAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String description;
	private final String sortKey;
	@Builder.Default
	@NonNull
	private final RequirementWeight weight = RequirementWeight.SHOULD_HAVE;
	@Builder.Default
	@NonNull
	private final String key = UUID.randomUUID().toString();

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddConstraintAspectCommandBuilder {
	}
}
