package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import java.util.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SetSystemIntegrationStepsCommandBuilder")
@JsonDeserialize(builder = SetSystemIntegrationStepsCommand.SetSystemIntegrationStepsCommandBuilder.class)
public class SetSystemIntegrationStepsCommand implements ViewCommand {
	@NonNull
	private final String key;
	@NonNull
	private final List<StepKeyValuePair> steps;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SetSystemIntegrationStepsCommandBuilder {
	}
}
