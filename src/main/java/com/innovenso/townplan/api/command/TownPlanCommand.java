package com.innovenso.townplan.api.command;

import com.innovenso.eventsourcing.api.command.EntityCommand;

public interface TownPlanCommand extends EntityCommand {
}
