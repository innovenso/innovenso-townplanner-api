package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateITSystemCommandBuilder")
@JsonDeserialize(builder = UpdateITSystemCommand.UpdateITSystemCommandBuilder.class)
public class UpdateITSystemCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String platform;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateITSystemCommandBuilder {
	}
}
