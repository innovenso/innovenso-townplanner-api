package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddFlowViewStepCommandBuilder")
@JsonDeserialize(builder = AddFlowViewStepCommand.AddFlowViewStepCommandBuilder.class)
public class AddFlowViewStepCommand implements ViewCommand {
	@NonNull
	private final String view;
	@NonNull
	private final String title;
	@NonNull
	private final String relationship;
	private final boolean response;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddFlowViewStepCommandBuilder {
	}
}
