package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateItProjectMilestoneCommandBuilder")
@JsonDeserialize(builder = UpdateItProjectMilestoneCommand.UpdateItProjectMilestoneCommandBuilder.class)
public class UpdateItProjectMilestoneCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String project;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateItProjectMilestoneCommandBuilder {
	}
}
