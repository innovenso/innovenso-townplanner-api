package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(builderClassName = "ClearAwsElementCommandBuilder")
@JsonDeserialize(builder = ClearAwsElementCommand.ClearAwsElementCommandBuilder.class)
public class ClearAwsElementCommand implements TownPlanCommand {

	@JsonPOJOBuilder(withPrefix = "")
	public static class ClearAwsElementCommandBuilder {
	}
}
