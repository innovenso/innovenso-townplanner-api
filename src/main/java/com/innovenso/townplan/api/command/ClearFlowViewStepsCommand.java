package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "ClearFlowViewStepsCommandBuilder")
@JsonDeserialize(builder = ClearFlowViewStepsCommand.ClearFlowViewStepsCommandBuilder.class)
public class ClearFlowViewStepsCommand implements ViewCommand {
	@NonNull
	private final String view;

	@JsonPOJOBuilder(withPrefix = "")
	public static class ClearFlowViewStepsCommandBuilder {
	}
}
