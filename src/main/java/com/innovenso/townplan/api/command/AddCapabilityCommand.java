package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddCapabilityCommandBuilder")
@JsonDeserialize(builder = AddCapabilityCommand.AddCapabilityCommandBuilder.class)
public class AddCapabilityCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final String enterprise;
	private final String parent;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddCapabilityCommandBuilder {
	}
}
