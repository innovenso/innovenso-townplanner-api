package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddITPlatformCommandBuilder")
@JsonDeserialize(builder = AddITPlatformCommand.AddITPlatformCommandBuilder.class)
public class AddITPlatformCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddITPlatformCommandBuilder {
	}
}
