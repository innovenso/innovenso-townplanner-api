package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddFlowViewCommandBuilder")
@JsonDeserialize(builder = AddFlowViewCommand.AddFlowViewCommandBuilder.class)
public class AddFlowViewCommand implements ViewCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	private final boolean showStepCounter;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddFlowViewCommandBuilder {
	}
}
