package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.SWOTType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddSwotAspectCommandBuilder")
@JsonDeserialize(builder = AddSwotAspectCommand.AddSwotAspectCommandBuilder.class)
public class AddSwotAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final SWOTType type;
	@NonNull
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddSwotAspectCommandBuilder {
	}
}
