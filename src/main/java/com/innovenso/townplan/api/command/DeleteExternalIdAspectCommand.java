package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ExternalIdType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteExternalIdAspectCommandBuilder")
@JsonDeserialize(builder = DeleteExternalIdAspectCommand.DeleteExternalIdAspectCommandBuilder.class)
public class DeleteExternalIdAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String value;
	@NonNull
	private final ExternalIdType type;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteExternalIdAspectCommandBuilder {
	}
}
