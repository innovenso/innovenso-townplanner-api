package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddImpactViewCapabilityCommandBuilder")
@JsonDeserialize(builder = AddImpactViewCapabilityCommand.AddImpactViewCapabilityCommandBuilder.class)
public class AddImpactViewCapabilityCommand implements ElementCommand {
	@NonNull
	private final String view;
	@NonNull
	private final String capability;
	private final boolean impacted;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddImpactViewCapabilityCommandBuilder {
	}
}
