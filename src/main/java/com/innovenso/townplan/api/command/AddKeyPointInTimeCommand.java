package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.time.LocalDate;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddKeyPointInTimeCommandBuilder")
@JsonDeserialize(builder = AddKeyPointInTimeCommand.AddKeyPointInTimeCommandBuilder.class)
public class AddKeyPointInTimeCommand implements TownPlanCommand {
	@NonNull
	private final LocalDate date;
	private String title;
	private boolean diagramsNeeded;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddKeyPointInTimeCommandBuilder {
	}
}
