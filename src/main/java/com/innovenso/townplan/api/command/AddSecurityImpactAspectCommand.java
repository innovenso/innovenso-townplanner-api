package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.SecurityImpactLevel;
import com.innovenso.townplan.api.value.aspects.SecurityImpactType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddSecurityImpactAspectCommandBuilder")
@JsonDeserialize(builder = AddSecurityImpactAspectCommand.AddSecurityImpactAspectCommandBuilder.class)
public class AddSecurityImpactAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final SecurityImpactType impactType;
	private final String description;
	@NonNull
	private final SecurityImpactLevel impactLevel;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddSecurityImpactAspectCommandBuilder {
	}
}
