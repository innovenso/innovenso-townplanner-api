package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.it.decision.DecisionOptionVerdict;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateDecisionOptionCommandBuilder")
@JsonDeserialize(builder = UpdateDecisionOptionCommand.UpdateDecisionOptionCommandBuilder.class)
public class UpdateDecisionOptionCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final String sortKey;
	@NonNull
	private final DecisionOptionVerdict verdict;

	@JsonPOJOBuilder(withPrefix = "")
	public static class UpdateDecisionOptionCommandBuilder {
	}
}
