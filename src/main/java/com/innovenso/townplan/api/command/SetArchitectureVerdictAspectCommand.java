package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "SetArchitectureVerdictAspectCommandBuilder")
@JsonDeserialize(builder = SetArchitectureVerdictAspectCommand.SetArchitectureVerdictAspectCommandBuilder.class)
public class SetArchitectureVerdictAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final ArchitectureVerdictType verdictType;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class SetArchitectureVerdictAspectCommandBuilder {
	}
}
