package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteDocumentationAspectCommandBuilder")
@JsonDeserialize(builder = DeleteDocumentationAspectCommand.DeleteDocumentationAspectCommandBuilder.class)
public class DeleteDocumentationAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String documentationKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteDocumentationAspectCommandBuilder {
	}
}
