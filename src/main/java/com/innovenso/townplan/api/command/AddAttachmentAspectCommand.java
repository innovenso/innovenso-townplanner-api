package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAttachmentAspectCommandBuilder")
@JsonDeserialize(builder = AddAttachmentAspectCommand.AddAttachmentAspectCommandBuilder.class)
public class AddAttachmentAspectCommand implements TownPlanCommand {
	@NonNull
	String key;
	@NonNull
	String conceptKey;
	@NonNull
	String cdnContentUrl;
	@NonNull
	String title;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAttachmentAspectCommandBuilder {
	}
}
