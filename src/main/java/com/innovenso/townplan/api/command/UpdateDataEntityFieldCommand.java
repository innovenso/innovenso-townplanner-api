package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = UpdateDataEntityFieldCommand.Builder.class)
public class UpdateDataEntityFieldCommand implements ElementCommand {
	@NonNull
	String key;
	@NonNull
	String title;
	String description;
	String sortKey;
	@NonNull
	String type;
	boolean mandatory;
	boolean unique;
	String fieldConstraints;
	String defaultValue;
	String localization;

	@JsonPOJOBuilder(withPrefix = "")
	public static class Builder {
	}
}
