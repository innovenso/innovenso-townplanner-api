package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "DeleteAspectCommandBuilder")
@JsonDeserialize(builder = DeleteAspectCommand.DeleteAspectCommandBuilder.class)
public class DeleteAspectCommand implements TownPlanCommand {
	@NonNull
	private final String conceptKey;
	@NonNull
	private final String aspectKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class DeleteAspectCommandBuilder {
	}
}
