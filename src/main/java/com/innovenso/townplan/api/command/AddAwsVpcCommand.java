package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsVpcCommandBuilder")
@JsonDeserialize(builder = AddAwsVpcCommand.AddAwsVpcCommandBuilder.class)
public class AddAwsVpcCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String region;
	@NonNull
	private final String networkMask;
	@NonNull
	private final String account;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsVpcCommandBuilder {
	}
}
