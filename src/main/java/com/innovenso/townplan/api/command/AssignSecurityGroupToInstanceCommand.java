package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AssignSecurityGroupToInstanceCommandBuilder")
@JsonDeserialize(builder = AssignSecurityGroupToInstanceCommand.AssignSecurityGroupToInstanceCommandBuilder.class)
public class AssignSecurityGroupToInstanceCommand implements TownPlanCommand {
	@NonNull
	private final String securityGroupKey;
	@NonNull
	private final String instanceKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AssignSecurityGroupToInstanceCommandBuilder {
	}
}
