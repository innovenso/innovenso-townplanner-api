package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddEnterpriseCommandBuilder")
@JsonDeserialize(builder = AddEnterpriseCommand.AddEnterpriseCommandBuilder.class)
public class AddEnterpriseCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddEnterpriseCommandBuilder {
	}
}
