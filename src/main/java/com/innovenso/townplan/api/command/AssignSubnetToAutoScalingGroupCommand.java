package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AssignSubnetToAutoScalingGroupCommandBuilder")
@JsonDeserialize(builder = AssignSubnetToAutoScalingGroupCommand.AssignSubnetToAutoScalingGroupCommandBuilder.class)
public class AssignSubnetToAutoScalingGroupCommand implements ElementCommand {
	@NonNull
	private final String subnet;
	@NonNull
	private final String autoscalingGroup;

	@Override
	public String getKey() {
		return "";
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class AssignSubnetToAutoScalingGroupCommandBuilder {
	}
}
