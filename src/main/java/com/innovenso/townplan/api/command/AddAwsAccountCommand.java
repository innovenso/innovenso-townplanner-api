package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsAccountCommandBuilder")
@JsonDeserialize(builder = AddAwsAccountCommand.AddAwsAccountCommandBuilder.class)
public class AddAwsAccountCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String accountId;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsAccountCommandBuilder {
	}
}
