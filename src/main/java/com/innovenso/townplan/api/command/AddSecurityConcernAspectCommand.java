package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.SecurityConcernType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddSecurityConcernAspectCommandBuilder")
@JsonDeserialize(builder = AddSecurityConcernAspectCommand.AddSecurityConcernAspectCommandBuilder.class)
public class AddSecurityConcernAspectCommand implements TownPlanCommand {
	@NonNull
	private final String modelComponentKey;
	@NonNull
	private final String title;
	private final String description;
	@NonNull
	private final SecurityConcernType concernType;
	private final String sortKey;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddSecurityConcernAspectCommandBuilder {
	}
}
