package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.enums.AwsInstanceType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsInstanceCommandBuilder")
@JsonDeserialize(builder = AddAwsInstanceCommand.AddAwsInstanceCommandBuilder.class)
public class AddAwsInstanceCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final AwsInstanceType instanceType;
	@NonNull
	private final String account;
	private final String vpc;
	private final String region;
	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsInstanceCommandBuilder {
	}
}
