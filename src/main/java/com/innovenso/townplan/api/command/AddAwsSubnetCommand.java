package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AddAwsSubnetCommandBuilder")
@JsonDeserialize(builder = AddAwsSubnetCommand.AddAwsSubnetCommandBuilder.class)
public class AddAwsSubnetCommand implements ElementCommand {
	@NonNull
	private final String key;
	@NonNull
	private final String title;
	@NonNull
	private final String subnetMask;
	@NonNull
	private final String vpc;
	@NonNull
	private final String availabilityZone;
	private final boolean publicSubnet;

	private final String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AddAwsSubnetCommandBuilder {
	}
}
