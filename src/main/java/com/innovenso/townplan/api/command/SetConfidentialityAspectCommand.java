package com.innovenso.townplan.api.command;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType;
import com.innovenso.townplan.api.value.aspects.ConfidentialityLevel;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "AspectCommandBuilder")
@JsonDeserialize(builder = SetConfidentialityAspectCommand.AspectCommandBuilder.class)
public class SetConfidentialityAspectCommand implements TownPlanCommand {
	@NonNull
	String conceptKey;
	@NonNull
	ConfidentialityLevel level;
	String description;

	@JsonPOJOBuilder(withPrefix = "")
	public static class AspectCommandBuilder {
	}
}
